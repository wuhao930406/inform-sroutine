import React, { useEffect, useMemo, useState } from 'react';
import { Text, View, Input } from '@tarojs/components';
import AutoList from '../../../components/AutoList';
import Taro, { useDidShow } from '@tarojs/taro';
import Detail from '../../../components/Detail';
import { useRequest } from 'ahooks';
import { request } from '../../../utils/request';
import { AtTabs, AtTabsPane, AtActionSheet, AtActionSheetItem } from 'taro-ui'
import "taro-ui/dist/style/components/tabs.scss" // 按需引入
import "taro-ui/dist/style/components/action-sheet.scss" // 按需引入


const Inspectiondetail = ({
    params,
}) => {
    const { id, type } = Taro.Current.router.params;
    const [current, setcurrent] = useState(0);
    const [isopen, setisopen] = useState(false);

    let { data, refresh } = useRequest(() => {
        return request(type == "done" ? "/ngic-app/wechatwork/equipmentCheck/queryHisDetail" : "/ngic-app/wechatwork/equipmentCheck/queryDetail", { id })
    })


    useDidShow(() => {
        refresh();
    })


    let res = useMemo(() => {
        if (data?.data) {
            return data?.data?.data
        } else {
            return {}
        }


    }, [data])

    let columna = [
        {
            "title": "工厂名称",
            "dataIndex": "shopName",
            "key": "shopName"
        },
        {
            "title": "车间名称",
            "dataIndex": "shopSectionName",
            "key": "shopSectionName"
        },
        {
            "title": "设备名称",
            "dataIndex": "equipmentName",
            "key": "equipmentName"
        },
        {
            "title": '设备位置号',
            "dataIndex": 'positionNo',
            "key": 'positionNo',
        },
        {
            "title": "设备编号",
            "dataIndex": "equipmentNo",
            "key": "equipmentNo"
        },
        {
            "title": "设备型号",
            "dataIndex": "equipmentModelName",
            "key": "equipmentModelName"
        },
    ], columnb = [
        {
            "title": "点检单号",
            "dataIndex": "taskNo",
            "key": "taskNo"
        },
        {
            "title": "工单状态",
            "dataIndex": "statusName",
            "key": "statusName"
        },
        {
            "title": "审核状态",
            "dataIndex": "approvalStatusName",
            "key": "approvalStatusName"
        },
        {
            "title": "点检截止日期",
            "dataIndex": "planCheckDate",
            "key": "planCheckDate"
        },
        {
            "title": "创建时间",
            "dataIndex": "createTime",
            "key": "createTime"
        },
        {
            "title": "点检计划单号",
            "dataIndex": "planNo",
            "key": "planNo"
        },
    ], columnc = [{
        "title": "点检人",
        "dataIndex": "checkUserName",
        "key": "checkUserName"
    },

    {
        "title": "点检开始时间",
        "dataIndex": "checkStartTime",
        "key": "checkStartTime"
    },
    {
        "title": "点检结束时间",
        "dataIndex": "checkEndTime",
        "key": "checkEndTime"
    },
    {
        "title": "备注",
        "dataIndex": "remark",
        "key": "remark"
    }
    ]

    let renderFirst = (<View>
        <Detail
            hasSplit={true}
            columns={columna}
            dataSource={
                res?.data ?? {}
            }
        ></Detail>

        <Detail
            columns={columnb}
            dataSource={
                res?.data ?? {}
            }
        ></Detail>
        {
            res?.checkItem?.item.length > 0 && <View className='body ttle'>
                点检项目
            </View>
        }
        {
            res?.checkItem?.item.map((it, i) => {
                return <View style={{ backgroundColor: "#fff", borderRadius: "8PX", margin: "6PX", padding: "12PX", borderBottom: i == res?.checkItem.item.length - 1 ? "none" : "1px solid #f0f0f0" }} key={it.id}>
                    <View className='body' style={{ marginBottom: 10, color: "#ff4800" }}>
                        {it?.checkItem}
                    </View>
                    <Text className='subbody'>
                        点检部位:{it?.checkPosition}
                    </Text>
                    <View className='spread' style={{ margin: "8px 0 0 0" }}>
                        <Text className='subbody'>下限值:{it?.lowerLimit ?? "-"}</Text>
                        <Text className='subbody'>上限值:{it?.upperLimit ?? "-"}</Text>
                    </View>
                </View>
            })
        }
    </View >),
        renderSecond = (
            <View>
                <Detail
                    columns={columnc}
                    dataSource={
                        res?.check ?? {}
                    }
                ></Detail>
                {
                    res?.checkItem?.item.length > 0 && <View className='body ttle'>
                        点检项目
                    </View>
                }
                {
                    res?.checkItem?.item.map((it, i) => {
                        return <View style={{ backgroundColor: "#fff", borderRadius: "8PX", margin: "6PX", padding: "12PX", borderBottom: i == res?.checkItem.item.length - 1 ? "none" : "1px solid #f0f0f0" }} key={it.id}>
                            <View className='body' style={{ marginBottom: 10, color: "#ff4800" }}>
                                {it?.checkItem}
                            </View>
                            <Text className='subbody'>
                                点检部位:{it?.checkPosition}
                            </Text>
                            <View className='spread' style={{ margin: "8px 0" }}>
                                <Text className='subbody'>下限值:{it?.lowerLimit ?? "-"}</Text>
                                <Text className='subbody'>上限值:{it?.upperLimit ?? "-"}</Text>
                            </View>
                            <View className='spread'>
                                <Text className='subbody'>点检结果:</Text>
                                <Text className='subbody'>
                                    {it?.judgeType == 1 ? (it?.judgeResultQualitative == 2 ? "异常" : it?.judgeResultQualitative == 1 ? "正常" : "") : it?.judgeResultRation}
                                </Text>
                            </View>
                            <View className='spread' style={{ margin: "8px 0" }}>
                                <Text className='subbody'>备注:{it?.remark ?? "-"}</Text>
                            </View>
                        </View>
                    })
                }
                <View className='body ttle'>
                    申请延期信息
                </View>
                <Detail
                    columns={[
                        {
                            "title": "申请人",
                            "dataIndex": "applyUserName",
                            "key": "applyUserName"
                        },
                        {
                            "title": "延期日期",
                            "dataIndex": "delayCheckDate",
                            "key": "delayCheckDate"
                        },
                        {
                            "title": "延期原因",
                            "dataIndex": "delayReason",
                            "key": "delayReason"
                        },
                        {
                            "title": "申请时间",
                            "dataIndex": "applyTime",
                            "key": "applyTime"
                        }
                    ]}
                    dataSource={
                        res?.apply ?? {}
                    }
                ></Detail>
                <View className='body ttle'>
                    延期审批信息
                </View>
                <Detail
                    columns={[
                        {
                            "title": "审核内容",
                            "dataIndex": "approvalComments",
                            "key": "approvalComments"
                        },
                        {
                            "title": "审核人",
                            "dataIndex": "approvalUserName",
                            "key": "approvalUserName"
                        },
                        {
                            "title": "审核时间",
                            "dataIndex": "approvalTime",
                            "key": "approvalTime"
                        },
                        {
                            "title": "审核结果",
                            "dataIndex": "approvalResultName",
                            "key": "approvalResultName"
                        }
                    ]}
                    dataSource={
                        res?.aduit ?? {}
                    }
                ></Detail>
            </View>
        )



    return (
        <View style={{ display: "flex", flexDirection: "column" }}>
            <AtActionSheet isOpened={isopen} onClose={() => {
                setisopen(false)
            }}>
                <AtActionSheetItem onClick={() => {
                    request("/ngic-app/wechatwork/equipmentCheck/acceptTask", { id }, (res) => {
                        if (res.code == "0000") {
                            Taro.showToast({
                                title: '操作成功',
                                icon: 'success',
                                duration: 2000
                            })
                            refresh();
                            setisopen(false)
                        }
                    })
                }}>
                    <View className='body' style={{ color: "#ff4800" }}>
                        确认接单
                    </View>
                </AtActionSheetItem>
                <AtActionSheetItem onClick={() => {
                    setisopen(false)
                }}>
                    <View className='body'>
                        取消
                    </View>
                </AtActionSheetItem>
            </AtActionSheet>
            <AtTabs
                current={current}
                scroll
                tabList={[
                    { title: '点检信息' },
                    { title: '执行信息' }
                ]}
                onClick={(val) => {
                    setcurrent(val)
                }}>
                <AtTabsPane current={current} index={0}>
                    {renderFirst}
                </AtTabsPane>
                <AtTabsPane current={current} index={1}>
                    {renderSecond}
                </AtTabsPane>

            </AtTabs>
            <View className='center' style={{ height: "48PX", width: "100%" }} />

            {
                (res?.data?.status == 1 && res?.data?.approvalStatus != 1 && type == "my") && <View className='center' style={{ position: "fixed", bottom: 0, height: "48PX", width: "100%", backgroundColor: "#f0f0f0" }}>
                    <View className='center' style={{ flex: 1, color: "#ff4800", fontSize: "16PX" }} onClick={() => {
                        setisopen(true)
                    }}>
                        接单
                    </View>
                    <View className='center' style={{ flex: 1, color: "#ff4800", fontSize: "16PX" }} onClick={() => {
                        Taro.navigateTo({ url: "/pages/actionform/index?type=inspection_delay&id=" + id })
                    }}>
                        延期申请
                    </View>
                </View>
            }
            {
                (res?.data?.status == 2 && type == "my") && <View className='center' style={{ position: "fixed", bottom: 0, height: "48PX", width: "100%", backgroundColor: "#f0f0f0" }}>
                    <View className='center' style={{ flex: 1, color: "#ff4800", fontSize: "16PX" }} onClick={() => {
                        Taro.navigateTo({ url: "/pages/actionform/index?type=inspection_finish&id=" + id })
                    }}>
                        完成点检
                    </View>
                </View>
            }

            {
                (res?.data?.approvalStatus == 1 && type == "my") && <View className='center' style={{ position: "fixed", bottom: 0, height: "48PX", width: "100%", backgroundColor: "#f0f0f0" }}>
                    <View className='center' style={{ flex: 1, color: "#ff4800", fontSize: "16PX" }} onClick={() => {
                        Taro.navigateTo({ url: "/pages/actionform/index?type=inspection_audit&id=" + id })
                    }}>
                        延期审批
                    </View>
                </View>
            }

        </View>

    )
};

export default Inspectiondetail;
