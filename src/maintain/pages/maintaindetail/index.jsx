import React, { useEffect, useMemo, useState } from 'react';
import { Text, View, Input } from '@tarojs/components';
import AutoList from '../../../components/AutoList';
import Taro, { useDidShow } from '@tarojs/taro';
import Detail from '../../../components/Detail';
import { useRequest } from 'ahooks';
import { request } from '../../../utils/request';
import { AtTabs, AtTabsPane, AtActionSheet, AtActionSheetItem, AtAccordion } from 'taro-ui'

import "taro-ui/dist/style/components/accordion.scss";
import "taro-ui/dist/style/components/tabs.scss" // 按需引入
import "taro-ui/dist/style/components/action-sheet.scss" // 按需引入


const Maintaindetail = ({
    params,
}) => {
    const { id, type } = Taro.Current.router.params;
    const [current, setcurrent] = useState(0);
    const [isopen, setisopen] = useState(false);
    const [open, setopen] = useState(true),
        [opens, setopens] = useState(false);
    let { data, refresh } = useRequest(() => {
        return request(type == "done" ? "/ngic-app/app/equipmentMaintain/queryHisDetail" : "/ngic-app/app/equipmentMaintain/queryDetailById", { id })
    })

    useDidShow(() => {
        refresh();
    })


    useEffect(() => {
        var pages = getCurrentPages();
        //当前页面
        var currPage = pages[pages.length - 1];
        let needRefresh = currPage?.data?.needRefresh;
        if (needRefresh) {

            // 防止每次进入该页面都要刷新
            currPage?.setData({ needRefresh: false })
        }
    }, [])

    let res = useMemo(() => {
        if (data?.data) {
            return data?.data?.data
        } else {
            return {}
        }
    }, [data])

    let columna = [
        {
            "title": "工厂名称",
            "dataIndex": "shopName",
            "key": "shopName"
        },
        {
            "title": "车间名称",
            "dataIndex": "shopSectionName",
            "key": "shopSectionName"
        },
        {
            "title": "设备名称",
            "dataIndex": "equipmentName",
            "key": "equipmentName"
        },
        {
            "title": '设备位置号',
            "dataIndex": 'positionNo',
            "key": 'positionNo',
        },
        {
            "title": "设备编号",
            "dataIndex": "equipmentNo",
            "key": "equipmentNo"
        },
        {
            "title": "设备型号",
            "dataIndex": "equipmentModelName",
            "key": "equipmentModelName"
        },
    ], columnb = [
        {
            "title": "保养单号",
            "dataIndex": "taskNo",
            "key": "taskNo"
        },
        {
            "title": "工单状态",
            "dataIndex": "taskStatusName",
            "key": "taskStatusName"
        },
        {
            "title": "审核状态",
            "dataIndex": "auditStatusName",
            "key": "auditStatusName"
        },
        {
            "title": "保养截止日期",
            "dataIndex": "planMaintainDate",
            "key": "planMaintainDate"
        },
        {
            "title": "创建时间",
            "dataIndex": "createTime",
            "key": "createTime"
        },
        {
            "title": "保养频率",
            "dataIndex": "maintainFrequencyName",
            "key": "maintainFrequencyName"
        }
    ], columnc = [{
        "title": "保养人",
        "dataIndex": "maintainUserName",
        "key": "maintainUserName"
    },

    {
        "title": "保养开始时间",
        "dataIndex": "maintainStartTime",
        "key": "maintainStartTime"
    },
    {
        "title": "保养结束时间",
        "dataIndex": "maintainEndTime",
        "key": "maintainEndTime"
    },
    {
        "title": "备注",
        "dataIndex": "remark",
        "key": "remark"
    }
    ]


    let renderFirst = (<View>
        <Detail
            hasSplit={true}
            columns={columna}
            dataSource={
                res?.data ?? {}
            }
        ></Detail>
        <Detail
            columns={columnb}
            dataSource={
                res?.data ?? {}
            }
        ></Detail>
        {
            res?.itemInfo?.item.length > 0 && <View className='body ttle'>
                保养项目
            </View>
        }
        {
            res?.itemInfo?.item.map((it, i) => {
                return <View style={{ backgroundColor: "#fff", borderRadius: "8PX", margin: "6PX", padding: "12PX", borderBottom: i == res?.itemInfo.item.length - 1 ? "none" : "1px solid #f0f0f0" }} key={it.id}>
                    <View className='body' style={{ marginBottom: 10, color: "#ff4800" }}>
                        {it?.maintainItemName}
                    </View>
                    <Text className='subbody'>
                        保养部位:{it?.maintainPosition}
                    </Text>
                    <View className='spread' style={{ margin: "8px 0 0 0" }}>
                        <Text className='subbody'>下限值:{it?.lowerLimit ?? "-"}</Text>
                        <Text className='subbody'>上限值:{it?.upperLimit ?? "-"}</Text>
                    </View>
                </View>
            })
        }
        <View style={{ backgroundColor: "#eee", height: 6 }}></View>
        <AtAccordion
            title='寿命件更换信息'
            open={open}
            onClick={(val) => { setopen(val) }}
            isAnimation={true}
        >
            {
                res?.data?.replaceList ? res?.data?.replaceList.map((it, i) => {
                    return <Detail
                        hasSplit={true}
                        columns={[
                            {
                                "title": "备件料号",
                                "dataIndex": "sparePartsNo",
                                "key": "sparePartsNo"
                            },
                            {
                                "title": "备件名",
                                "dataIndex": "sparePartsName",
                                "key": "sparePartsName"
                            },
                            {
                                "title": "安装部位",
                                "dataIndex": "position",
                                "key": "position"
                            },
                            {
                                "title": "下次更换日期",
                                "dataIndex": "nextReplaceDate",
                                "key": "nextReplaceDate"
                            },
                            {
                                "title": "安装数量",
                                "dataIndex": "installNum",
                                "key": "installNum"
                            },
                            {
                                "title": "更换数量",
                                "dataIndex": "num",
                                "key": "num"
                            }
                        ]}
                        dataSource={
                            it ?? {}
                        }
                    ></Detail>
                }) : <View className='center' style={{ padding: 12, color: "#999" }}>暂无数据</View>
            }
        </AtAccordion>
        <AtAccordion
            title='非寿命件更换信息'
            isAnimation={true}
            open={opens}
            onClick={(val) => { setopens(val) }}
        >
            {
                res?.data?.consumeList && res?.data?.consumeList.map((it, i) => {
                    return <Detail
                        hasSplit={true}
                        columns={[
                            {
                                "title": "备件料号",
                                "dataIndex": "sparePartsNo",
                                "key": "sparePartsNo"
                            },
                            {
                                "title": "备件名",
                                "dataIndex": "sparePartsName",
                                "key": "sparePartsName"
                            },
                            {
                                "title": "更换数量",
                                "dataIndex": "num",
                                "key": "num"
                            }
                        ]}
                        dataSource={
                            it ?? {}
                        }
                    ></Detail>
                })
            }

        </AtAccordion>
    </View >),
        renderSecond = (
            <View>
                <Detail
                    columns={columnc}
                    dataSource={
                        res?.maintainInfo ?? {}
                    }
                ></Detail>
                {
                    res?.itemInfo?.item.length > 0 && <View className='body ttle'>
                        保养项目
                    </View>
                }
                {
                    res?.itemInfo?.item.map((it, i) => {
                        return <View style={{ backgroundColor: "#fff", borderRadius: "8PX", margin: "6PX", padding: "12PX", borderBottom: i == res?.itemInfo.item.length - 1 ? "none" : "1px solid #f0f0f0" }} key={it.id}>
                            <View className='body' style={{ marginBottom: 10, color: "#ff4800" }}>
                                {it?.maintainItemName}
                            </View>
                            <Text className='subbody'>
                                保养部位:{it?.maintainPosition}
                            </Text>
                            <View className='spread' style={{ margin: "8px 0" }}>
                                <Text className='subbody'>下限值:{it?.lowerLimit ?? "-"}</Text>
                                <Text className='subbody'>上限值:{it?.upperLimit ?? "-"}</Text>
                            </View>
                            <View className='spread'>
                                <Text className='subbody'>保养结果:</Text>
                                <Text className='subbody'>
                                    {it?.judgeType == 1 ? (it?.judgeResultType == 1 ? "正常" : "异常") : it?.qualitativeJudgeResult}
                                </Text>
                            </View>
                            <View className='spread' style={{ margin: "8px 0" }}>
                                <Text className='subbody'>备注:{it?.remark ?? "-"}</Text>
                            </View>
                        </View>
                    })
                }

                <View className='body ttle'>
                    申请延期信息
                </View>
                <Detail
                    columns={[
                        {
                            "title": "申请人",
                            "dataIndex": "applyUserName",
                            "key": "applyUserName"
                        },
                        {
                            "title": "延期日期",
                            "dataIndex": "delayMaintainDate",
                            "key": "delayMaintainDate"
                        },
                        {
                            "title": "延期原因",
                            "dataIndex": "delayReason",
                            "key": "delayReason"
                        },
                        {
                            "title": "申请时间",
                            "dataIndex": "applyDateTime",
                            "key": "applyDateTime"
                        }
                    ]}
                    dataSource={
                        res?.applyInfo ?? {}
                    }
                ></Detail>
                <View className='body ttle'>
                    延期审批信息
                </View>
                <Detail
                    columns={[
                        {
                            "title": "审核内容",
                            "dataIndex": "auditComments",
                            "key": "auditComments"
                        },
                        {
                            "title": "审核人",
                            "dataIndex": "auditUserName",
                            "key": "auditUserName"
                        },
                        {
                            "title": "审核时间",
                            "dataIndex": "auditDateTime",
                            "key": "auditDateTime"
                        },
                        {
                            "title": "审核结果",
                            "dataIndex": "auditResultName",
                            "key": "auditResultName"
                        }
                    ]}
                    dataSource={
                        res?.auditInfo ?? {}
                    }
                ></Detail>
            </View>
        ), record = res?.data ?? {},
        ifs = record.taskStatus == 1,
        ifss = record.auditStatus == 1,
        ifc = record.taskStatus != 1,
        ifsc = record.auditStatus != 1,
        ifsa = record.taskStatus == 5;



    return (
        <View style={{ display: "flex", flexDirection: "column" }}>
            <AtActionSheet isOpened={isopen} onClose={() => {
                setisopen(false)
            }}>
                <AtActionSheetItem onClick={() => {
                    request("/ngic-app/app/equipmentMaintain/acceptTask", { id }, (res) => {
                        if (res.code == "0000") {
                            Taro.showToast({
                                title: '操作成功',
                                icon: 'success',
                                duration: 2000
                            })
                            refresh();
                            setisopen(false)
                        }
                    })
                }}>
                    <View className='body' style={{ color: "#ff4800" }}>
                        确认接单
                    </View>
                </AtActionSheetItem>
                <AtActionSheetItem onClick={() => {
                    setisopen(false)
                }}>
                    <View className='body'>
                        取消
                    </View>
                </AtActionSheetItem>
            </AtActionSheet>
            <AtTabs
                current={current}
                scroll
                tabList={[
                    { title: '保养信息' },
                    { title: '执行信息' }
                ]}
                onClick={(val) => {
                    setcurrent(val)
                }}>
                <AtTabsPane current={current} index={0}>
                    {renderFirst}
                </AtTabsPane>
                <AtTabsPane current={current} index={1}>
                    {renderSecond}
                </AtTabsPane>

            </AtTabs>
            <View className='center' style={{ height: "48PX", width: "100%" }} />

            {
                (ifs && res?.data?.auditStatus != 1 && type == "my") && <View className='center' style={{ position: "fixed", bottom: 0, height: "48PX", width: "100%", backgroundColor: "#f0f0f0" }}>
                    <View className='center' style={{ flex: 1, color: "#ff4800", fontSize: "16PX" }} onClick={() => {
                        setisopen(true)
                    }}>
                        接单
                    </View>
                    {
                        (ifs && ifsc) && <View className='center' style={{ flex: 1, color: "#ff4800", fontSize: "16PX" }} onClick={() => {
                            Taro.navigateTo({ url: "/pages/actionform/index?type=maintain_delay&id=" + id })
                        }}>
                            延期申请
                        </View>
                    }
                </View>
            }

            {
                (ifc && !ifsa && type == "my") && <View className='center' style={{ position: "fixed", bottom: 0, height: "48PX", width: "100%", backgroundColor: "#f0f0f0" }}>
                    <View className='center' style={{ flex: 1, color: "#ff4800", fontSize: "16PX" }} onClick={() => {
                        Taro.navigateTo({ url: `/pages/actionform/index?type=maintain_finish&id=${id}&equipmentId=${res?.data?.equipmentId}` })
                    }}>
                        完成保养
                    </View>
                </View>
            }

            {
                (ifss && type == "my") && <View className='center' style={{ position: "fixed", bottom: 0, height: "48PX", width: "100%", backgroundColor: "#f0f0f0" }}>
                    <View className='center' style={{ flex: 1, color: "#ff4800", fontSize: "16PX" }} onClick={() => {
                        Taro.navigateTo({ url: "/pages/actionform/index?type=maintain_audit&id=" + id })
                    }}>
                        延期审批
                    </View>
                </View>
            }

            {
                (ifsa && type == "my") && <View className='center' style={{ position: "fixed", bottom: 0, height: "48PX", width: "100%", backgroundColor: "#f0f0f0" }}>
                    <View className='center' style={{ flex: 1, color: "#ff4800", fontSize: "16PX" }} onClick={() => {
                        Taro.navigateTo({ url: "/pages/actionform/index?type=maintain_approval&id=" + id })
                    }}>
                        验证
                    </View>
                </View>
            }

        </View>

    )
};

export default Maintaindetail;
