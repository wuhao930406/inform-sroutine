import React from 'react';
import { View, Input } from '@tarojs/components';
import AutoList from '../../../components/AutoList';
import Taro from '@tarojs/taro';




const Maintainundo = ({
    params,
}) => {

    return (
        <View >
            <AutoList
                path="/ngic-app/app/equipmentMaintain/queryAllUndone"
                row={(item, i) => {
                    return <View className='item' onClick={()=>{
                        Taro.navigateTo({url:`/maintain/pages/maintaindetail/index?id=${item.id}&type=undo`})
                    }}>
                        <View style={{ display: "flex", justifyContent: "space-between", alignItems: "center", borderBottom: "1PX solid #f0f0f0", padding: "0PX 0 10PX 0", marginBottom: 10 }}>
                            <View>工单:{item.taskNo}</View>
                            <View className={`status_${item.taskStatus}`}>{item.taskStatusName}</View>
                        </View>
                        <View className='center' style={{ flexDirection: "column", width: "100%",fontSize:15 }}>
                            <View className='center' style={{ justifyContent: "space-between", width: "100%" }}>
                            <View>
                                    {item.equipmentName}
                                </View>
                                <View style={{marginLeft:12}}>
                                    {item.equipmentNo}
                                </View>
                            </View>
                            <View className='center' style={{ justifyContent: "space-between", width: "100%", marginTop: 6 }}>
                                <View>
                                    保养截止日期：{item.planMaintainDate}
                                </View>
                                <View className={`status_${item.auditStatus}`}>
                                    {item.auditStatusName}
                                </View>
                            </View>

                        </View>
                    </View>
                }}
            />
        </View >
    )
};

export default Maintainundo;
