import React, { useState } from 'react';
import { View, Image } from '@tarojs/components';
import Detail from '../../../components/Detail';
import { imgurl } from '../../../utils/pathconfig';
import { AtAccordion } from 'taro-ui'
import "taro-ui/dist/style/components/accordion.scss";

function RThird({ res }) {
    return (
        <View style={{ minHeight: "80vh" }}>
            {
                res?.repairInfo?.isShow && <View className='body ttle'>
                    维修信息
                </View>
            }
            {
                res?.repairInfo?.isShow && <Detail
                    columns={[
                        {
                            "title": "派单人员",
                            "key": "assignUserName"
                        },
                        {
                            "title": "接单时间",
                            "key": "acceptRepairTime"
                        },
                        {
                            "title": "开始维修时间",
                            "key": "startRepairTime"
                        },
                        {
                            "title": "完成维修时间",
                            "key": "finishRepairTime"
                        },
                        {
                            "title": "故障原因",
                            "key": "faultReason",
                        },
                        {
                            "title": "维修类型",
                            "key": "repairTypeName"
                        },
                        {
                            "title": "是否停机维修",
                            "key": "isStopRepair"
                        },
                        {
                            "title": "维修内容",
                            "key": "repairContent",
                        }
                    ]}
                    dataSource={res?.repairInfo ?? {}}
                />
            }
            {
                res?.repairInfo?.assistUsers && <View className='body ttle'>
                    协助维修信息
                </View>
            }
            {
                res?.repairInfo?.assistUsers &&
                res?.repairInfo?.assistUsers.map(it => {
                    return <Detail
                        columns={[
                            {
                                "title": "维修人员",
                                "dataIndex": "repairUserName",
                                "key": "repairUserName"
                            },
                            {
                                "title": "预计工时",
                                "dataIndex": "workingHours",
                                "key": "workingHours"
                            },
                            {
                                "title": "实际工时(验证)",
                                "dataIndex": "actualWorkHours",
                                "key": "actualWorkHours"
                            },
                            {
                                "title": "实际工时(确认)",
                                "dataIndex": "actualWorkHours2",
                                "key": "actualWorkHours2"
                            }
                        ]}
                        dataSource={
                            it
                        }
                    ></Detail>
                })
            }

            {
                res?.handInfo?.isShow && <>
                    <View className='body ttle'>
                        交接班信息
                    </View>
                    <Detail
                        columns={[
                            {
                                title: "交接人",
                                key: "handRepairUserName"
                            },
                            {
                                title: "交接时间",
                                key: "handoverTime"
                            },
                            {
                                title: "交接内容",
                                key: "handoverContent"
                            },
                        ]}
                        dataSource={
                            res?.handInfo ?? {}
                        }
                    ></Detail>
                </>
            }

            {
                res?.applyOuthelpInfo?.isShow && <>
                    <View className='body ttle'>
                        请求外协信息
                    </View>
                    <Detail
                        columns={[
                            {
                                title: "请求外协原因",
                                key: "applyOuthelpReason"
                            }, {
                                title: "请求外协时间",
                                key: "applyOuthelpTime"
                            }
                        ]}
                        dataSource={
                            res?.applyOuthelpInfo ?? {}
                        }
                    ></Detail>

                </>
            }
            {
                res?.returnInfo?.isShow && <>
                    <View className='body ttle'>
                        退回预判信息
                    </View>
                    <Detail
                        columns={[
                            {
                                title: "退回预判原因",
                                key: "returnPredictionReason"
                            }, {
                                title: "退回预判时间",
                                key: "applyOuthelpTime"
                            }
                        ]}
                        dataSource={
                            res?.returnInfo ?? {}
                        }
                    ></Detail>


                </>
            }



            {
                res?.returnInfo?.isShow && <>
                    <View className='body ttle'>
                        退单信息
                    </View>
                    <Detail
                        columns={[
                            {
                                title: "退单理由",
                                key: "returnOrderReason"
                            }, {
                                title: "退单时间",
                                key: "returnOrderTime"
                            }
                        ]}
                        dataSource={
                            res?.returnInfo ?? {}
                        }
                    ></Detail>


                </>
            }


            {
                res?.assistInfo?.isShow && <>

                    <View className='body ttle'>
                        申请援助
                    </View>
                    <Detail
                        columns={[
                            {
                                title: "申请援助原因",
                                key: "applyAssistReason"
                            },
                            {
                                title: "申请援助时间",
                                key: "applyHelpTime"
                            },
                        ]}
                        dataSource={
                            res?.assistInfo ?? {}
                        }
                    ></Detail>

                </>}

            {
                res?.confirmInfo?.isShow && <>

                    <View className='body ttle'>
                        验收信息
                    </View>
                    <Detail
                        columns={[
                            {
                                title: "验证人员",
                                key: "confirmUserName"
                            },
                            {
                                title: "验证时间",
                                key: "confirmTime"
                            },
                            {
                                title: "验证结果",
                                key: "confirmIsPassName"
                            },
                            {
                                title: "说明",
                                key: "confirmDesc"
                            },
                            {
                                title: "验证文件",
                                key: "confirmFileIdList",
                                "columns": true,
                                "render": (text, row) => {
                                    return <View style={{ display: "flex", justifyContent: "flex-start", alignItems: "center", flexWrap: "wrap", marginTop: 12, width: "110vw" }}>
                                        {
                                            row?.confirmFileIdList?.map((it, i) => {
                                                return <Image onClick={() => {
                                                    Taro.previewImage({
                                                        current: imgurl + it.url,
                                                        urls: row?.confirmFileIdList?.map(its => imgurl + its.url)
                                                    })
                                                }} src={imgurl + it.url} key={i} style={{ width: "20vw", height: "20vw", marginRight: "2vw", marginBottom: "2vw" }}></Image>
                                            })
                                        }
                                    </View>
                                }
                            }
                        ]
                        }
                        dataSource={
                            res?.confirmInfo ?? {}
                        }
                    ></Detail>

                </>
            }
            {
                res?.confirm2Info?.isShow && <>

                    <View className='body ttle'>
                        确认信息
                    </View>
                    <Detail
                        columns={[
                            {
                                title: "确认人员",
                                key: "confirm2UserName"
                            },
                            {
                                title: "确认时间",
                                key: "confirm2Time"
                            },
                            {
                                title: "确认结果",
                                key: "confirm2IsPassName"
                            },
                            {
                                title: "说明",
                                key: "confirm2Desc"
                            },
                            {
                                title: "确认文件",
                                key: "confirm2FileIdList",
                                "columns": true,
                                "render": (text, row) => {
                                    return <View style={{ display: "flex", justifyContent: "flex-start", alignItems: "center", flexWrap: "wrap", marginTop: 12, width: "110vw" }}>
                                        {
                                            row?.confirmFileIdList?.map((it, i) => {
                                                return <Image onClick={() => {
                                                    Taro.previewImage({
                                                        current: imgurl + it.url,
                                                        urls: row?.confirm2FileIdList?.map(its => imgurl + its.url)
                                                    })
                                                }} src={imgurl + it.url} key={i} style={{ width: "20vw", height: "20vw", marginRight: "2vw", marginBottom: "2vw" }}></Image>
                                            })
                                        }
                                    </View>
                                }
                            }
                        ]
                        }
                        dataSource={
                            res?.confirm2Info ?? {}
                        }
                    ></Detail>

                </>
            }
        </View>
    );
}

function RSecond({ res }) {
    const [open, setopen] = useState(true),
        [opens, setopens] = useState(false);
    return (
        <View style={{ minHeight: "80vh" }}>
            {
                res?.predictionInfo?.isShow && <>
                    <Detail
                        columns={[
                            {
                                "title": "预判时间",
                                "dataIndex": "predictionTime",
                                "key": "predictionTime"
                            },
                            {
                                "title": "预判人",
                                "dataIndex": "predictionUserName",
                                "key": "predictionUserName"
                            },
                            {
                                "title": "预计工时",
                                "dataIndex": "workingHours",
                                "key": "workingHours"
                            },
                            {
                                "title": "维修方案",
                                "dataIndex": "repairScheme",
                                "key": "repairScheme"
                            }
                        ]}
                        dataSource={
                            res?.predictionInfo ?? {}
                        }
                    ></Detail>
                    <View style={{ backgroundColor: "#eee", height: 6 }}></View>
                    <AtAccordion
                        title='寿命件更换信息'
                        open={open}
                        onClick={(val) => { setopen(val) }}
                        isAnimation={true}
                    >
                        {
                            res?.predictionInfo?.replaceList ? res?.predictionInfo?.replaceList.map((it, i) => {
                                return <Detail
                                    hasSplit={true}
                                    columns={[
                                        {
                                            "title": "备件料号",
                                            "dataIndex": "sparePartsNo",
                                            "key": "sparePartsNo"
                                        },
                                        {
                                            "title": "备件名",
                                            "dataIndex": "sparePartsName",
                                            "key": "sparePartsName"
                                        },
                                        {
                                            "title": "安装部位",
                                            "dataIndex": "position",
                                            "key": "position"
                                        },
                                        {
                                            "title": "下次更换日期",
                                            "dataIndex": "nextReplaceDate",
                                            "key": "nextReplaceDate"
                                        },
                                        {
                                            "title": "安装数量",
                                            "dataIndex": "installNum",
                                            "key": "installNum"
                                        },
                                        {
                                            "title": "更换数量",
                                            "dataIndex": "num",
                                            "key": "num"
                                        }
                                    ]}
                                    dataSource={
                                        it ?? {}
                                    }
                                ></Detail>
                            }) : <View className='center' style={{ padding: 12, color: "#999" }}>暂无数据</View>
                        }
                    </AtAccordion>
                    <AtAccordion
                        title='非寿命件更换信息'
                        isAnimation={true}
                        open={opens}
                        onClick={(val) => { setopens(val) }}
                    >
                        {
                            res?.predictionInfo?.consumeList && res?.predictionInfo?.consumeList.map((it, i) => {
                                return <Detail
                                    hasSplit={true}

                                    columns={[
                                        {
                                            "title": "备件料号",
                                            "dataIndex": "sparePartsNo",
                                            "key": "sparePartsNo"
                                        },
                                        {
                                            "title": "备件名",
                                            "dataIndex": "sparePartsName",
                                            "key": "sparePartsName"
                                        },
                                        {
                                            "title": "更换数量",
                                            "dataIndex": "num",
                                            "key": "num"
                                        }
                                    ]}
                                    dataSource={
                                        it ?? {}
                                    }
                                ></Detail>
                            })
                        }

                    </AtAccordion>

                </>}



        </View>
    );
}

function TSecond({ res }) {
    const { checkInfo } = res;
    const umEquipmentHelpDetailList = checkInfo?.umEquipmentHelpDetailList ?? [];
    return (
        <View style={{ minHeight: "80vh" }}>
            {
                res?.taskInfo?.isShow && <>
                    <View className='body ttle'>
                        接单信息
                    </View>
                    <Detail
                        columns={[
                            {
                                "title": "接单时间",
                                "dataIndex": "acceptTime",
                                "key": "acceptTime",
                            },
                        ]}
                        dataSource={
                            res?.taskInfo ?? {}
                        }
                    >
                    </Detail>

                </>
            }

            {
                res?.checkInfo?.isShow && <>
                    <View className='body ttle'>
                        检查信息
                    </View>
                    {
                        umEquipmentHelpDetailList && umEquipmentHelpDetailList.map((it, i) => {
                            return <Detail
                                columns={[
                                    {
                                        "title": "检查人员",
                                        "dataIndex": "checkUserName",
                                        "key": "checkUserName",
                                    },
                                    {
                                        "title": "实际追踪时间",
                                        "dataIndex": "actualTraceTime",
                                        "key": "actualTraceTime",
                                    },
                                    {
                                        "title": "追踪内容",
                                        "dataIndex": "remark",
                                        "key": "remark",
                                    }
                                ]}
                                dataSource={it}
                            ></Detail>
                        })
                    }

                </>
            }
            {
                res?.confirmInfo?.isShow && <>
                    <View className='body ttle'>
                        验证信息
                    </View>
                    <Detail
                        columns={[
                            {
                                title: "验证人员",
                                key: "confirmUserName"
                            },
                            {
                                title: "验证时间",
                                key: "confirmTime"
                            },
                            {
                                title: "验证结果",
                                key: "confirmIsPassName"
                            },
                            {
                                title: "说明",
                                key: "confirmDesc"
                            },
                            {
                                title: "验证文件",
                                key: "confirmFileIdList",
                                col: { span: 24 },
                                "render": (text, row) => {
                                    return <View style={{ display: "flex", justifyContent: "flex-start", alignItems: "center", flexWrap: "wrap", marginTop: 12, width: "110vw" }}>
                                        {
                                            row?.confirmFileIdList?.map((it, i) => {
                                                return <Image onClick={() => {
                                                    Taro.previewImage({
                                                        current: imgurl + it?.url,
                                                        urls: row?.confirmFileIdList?.map(its => imgurl + its.url)
                                                    })
                                                }} src={imgurl + it.url} key={i} style={{ width: "20vw", height: "20vw", marginRight: "2vw", marginBottom: "2vw" }}></Image>
                                            })
                                        }
                                    </View>
                                }
                            }
                        ]}
                        dataSource={
                            res?.confirmInfo ?? {}
                        }
                    >

                    </Detail>
                </>}



        </View>
    );
}

function HSecond({ res }) {
    return (
        <View style={{ minHeight: "80vh" }}>
            {
                res?.repairInfo?.isShow && <>
                    <View className='body ttle'>
                        接单信息
                    </View>
                    <Detail
                        columns={[
                            {
                                "title": "接单人员",
                                "key": "repairUserName"
                            },
                            {
                                "title": "接单时间",
                                "key": "acceptRepairTime"
                            },
                        ]}
                        dataSource={
                            res?.repairInfo ?? {}
                        }
                    >
                    </Detail>

                </>
            }

            {
                res?.repairInfo?.isShow && <>
                    <View className='body ttle'>
                        维修信息
                    </View>
                    <Detail
                        columns={[
                            {
                                "title": "开始维修时间",
                                "key": "startRepairTime"
                            },
                            {
                                "title": "完成维修时间",
                                "key": "finishRepairTime"
                            },
                            {
                                "title": "外协费用",
                                "key": "helpCost"
                            },
                            {
                                "title": "外协时间（小时）",
                                "key": "repairTime"
                            },
                            {
                                "title": "故障原因",
                                "key": "faultReason",
                            },
                            {
                                "title": "维修内容",
                                "key": "repairContent",
                            },
                            {
                                "title": "备注",
                                "key": "remark",
                            }
                        ]
                        }
                        dataSource={
                            res?.repairInfo ?? {}
                        }
                    >

                    </Detail>
                </>}



        </View>
    );
}


export { RSecond, RThird, TSecond, HSecond };