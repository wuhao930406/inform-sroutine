import React, { useEffect, useMemo, useState } from 'react';
import { View, Image } from '@tarojs/components';
import Taro, { useDidShow } from '@tarojs/taro';
import Detail from '../../../components/Detail';
import { useRequest } from 'ahooks';
import { request } from '../../../utils/request';
import { AtTabs, AtTabsPane, AtActionSheet, AtActionSheetItem } from 'taro-ui';
import { RThird, RSecond, TSecond, HSecond } from './repairtab';
import { imgurl } from '../../../utils/pathconfig';
import "taro-ui/dist/style/components/tabs.scss" // 按需引入
import "taro-ui/dist/style/components/action-sheet.scss" // 按需引入
import dayjs from 'dayjs';

let typeto = {
    repair: {
        path: "/ngic-app/repair/repair/queryTaskOrHisByTaskNo"
    },
    trace: {
        path: "/ngic-app/repair/trace/queryTaskOrHisByTaskNo"
    },
    help: {
        path: "/ngic-app/repair/help/queryTaskOrHisByTaskNo"
    }
}

const Repairdetail = ({
    params,
}) => {
    const { taskNo, type, action, id, applyRepairId, sectionId } = Taro.Current.router.params;
    const [current, setcurrent] = useState(0);
    const [isopen, setisopen] = useState(false);
    const [actions, setaction] = useState();

    let { data, refresh } = useRequest(() => {
        return request(typeto[type].path ?? "", { taskNo })
    })

    useDidShow(() => {
        refresh();
    })


    useEffect(() => {
        var pages = getCurrentPages();
        //当前页面
        var currPage = pages[pages.length - 1];
        let needRefresh = currPage?.data?.needRefresh;
        if (needRefresh) {

            // 防止每次进入该页面都要刷新
            currPage?.setData({ needRefresh: false })
        }
    }, [])

    let res = useMemo(() => {
        if (data?.data) {
            return data?.data?.data
        } else {
            return {}
        }
    }, [data]),
        record = res?.taskInfo ?? {},
        records = res?.faultInfo ?? {},
        recordc = { ...record, ...records };
    //报修单状态
    let ifscw = record.repairStatus == 82,
        ifsa = record.bStatus == 3,
        ifsb = record.bStatus == 4,
        ifsc = record.bStatus == 8,
        ifsd = record.bStatus == 2,
        ifse = record.bStatus == 9;
    //追踪单状态
    let zzifs = record.status == 1,
        zzifsc = record.status == 2,
        zzifss = record.status == 3
    //外协单状态
    let wxifs = record.status == 1,
        wxifsc = record.status == 2

    let columna = [
        {
            "title": "工厂名称",
            "dataIndex": "shopName",
            "key": "shopName"
        },
        {
            "title": "车间名称",
            "dataIndex": "sectionName",
            "key": "sectionName"
        },
        {
            "title": "设备名称",
            "dataIndex": "equipmentName",
            "key": "equipmentName"
        },
        {
            "title": '设备位置号',
            "dataIndex": 'positionNo',
            "key": 'positionNo',
        },
        {
            "title": "设备编号",
            "dataIndex": "equipmentNo",
            "key": "equipmentNo"
        },
        {
            "title": "设备型号",
            "dataIndex": "equipmentModelName",
            "key": "equipmentModelName"
        }
    ], columnb = [
        {
            "title": "维修单号",
            "dataIndex": "taskNo",
            "key": "taskNo"
        },
        {
            "title": "工单状态",
            "dataIndex": "statusName",
            "key": "statusName"
        },
        {
            "title": "来源单号",
            "dataIndex": "sourceNo",
            "key": "sourceNo"
        },
        {
            "title": "来源类型",
            "dataIndex": "sourceTypeName",
            "key": "sourceTypeName"
        },
        {
            "title": "报修人员",
            "dataIndex": "applyRepairUserName",
            "key": "applyRepairUserName"
        },
        {
            "title": "报修时间",
            "dataIndex": "applyRepairTime",
            "key": "applyRepairTime"
        },
        // {
        //     "title": "故障发生时间",
        //     "dataIndex": "faultOccurTime",
        //     "key": "faultOccurTime"
        // },
        {
            "title": "是否停机",
            "dataIndex": "isStopName",
            "key": "isStopName"
        },
        {
            "title": "故障分类",
            "dataIndex": "equipmentFaultName",
            "key": "equipmentFaultName"
        },
        {
            "title": "故障名称",
            "dataIndex": "equipmentFaultDetailName",
            "key": "equipmentFaultDetailName"
        },
        {
            "title": "故障描述",
            "dataIndex": "faultDesc",
            "key": "faultDesc"
        },
        {
            "title": "故障图片",
            "dataIndex": "faultImgList",
            "key": "faultImgList",
            "columns": true,
            "render": (text, row) => {
                return <View style={{ display: "flex", justifyContent: "flex-start", alignItems: "center", flexWrap: "wrap", marginTop: 12, width: "110vw" }}>
                    {
                        row?.faultImgList?.map((it, i) => {
                            return <Image onClick={() => {
                                Taro.previewImage({
                                    current: imgurl + it.url,
                                    urls: row?.faultImgList?.map(its => imgurl + its?.url)
                                })
                            }} src={imgurl + it.url} key={i} style={{ width: "20vw", height: "20vw", marginRight: "2vw", marginBottom: "2vw" }}></Image>
                        })
                    }
                </View>
            }
        }
    ]


    let renderFirst = (<View>
        <Detail
            hasSplit={true}
            columns={columna}
            dataSource={
                res?.baseInfo ?? {}
            }
        ></Detail>

        <Detail
            columns={columnb}
            dataSource={recordc}
        ></Detail>
    </View >);

    return (
        <View style={{ display: "flex", flexDirection: "column" }}>
            <AtActionSheet isOpened={isopen} onClose={() => {
                setisopen(false)
            }}>
                {
                    actions == "repair_delete" && <AtActionSheetItem onClick={() => {
                        request("/ngic-app/repair/deleteRepair", { applyRepairId }, (res) => {
                            if (res.code == "0000") {
                                Taro.showToast({
                                    title: '操作成功',
                                    icon: 'success',
                                    duration: 2000
                                })
                                var pages = Taro.getCurrentPages();
                                var prevPage = pages[pages.length - 2]; //上1个页面
                                prevPage.refresh = true;
                                Taro.navigateBack();
                            }
                        })
                    }}>
                        <View className='body' style={{ color: "#ff4800" }}>
                            确认删除
                        </View>
                    </AtActionSheetItem>
                }
                {
                    actions == "accept" && <AtActionSheetItem onClick={() => {
                        request("/ngic-app/repair/trace/accept", { id }, (res) => {
                            if (res.code == "0000") {
                                Taro.showToast({
                                    title: '操作成功',
                                    icon: 'success',
                                    duration: 2000
                                })
                                refresh();
                                setisopen(false)
                            }
                        })
                    }}>
                        <View className='body' style={{ color: "#6190E8" }}>
                            确认接单
                        </View>
                    </AtActionSheetItem>
                }

                {
                    actions == "helpaccept" && <AtActionSheetItem onClick={() => {
                        request("/ngic-app/repair/help/accept", { id }, (res) => {
                            if (res.code == "0000") {
                                Taro.showToast({
                                    title: '操作成功',
                                    icon: 'success',
                                    duration: 2000
                                })
                                refresh();
                                setisopen(false)
                            }
                        })
                    }}>
                        <View className='body' style={{ color: "#6190E8" }}>
                            确认接单
                        </View>
                    </AtActionSheetItem>
                }

                {
                    actions == "action" && <AtActionSheetItem onClick={() => {
                        Taro.navigateTo({ url: `/pages/actionform/index?type=repair_applyhelp&id=${id}` });
                        setisopen(false)
                    }}>
                        <View className='body' style={{ color: "#6190E8" }}>
                            申请援助
                        </View>
                    </AtActionSheetItem>
                }
                {
                    actions == "action" && <AtActionSheetItem onClick={() => {
                        Taro.navigateTo({ url: `/pages/actionform/index?type=repair_hand&id=${id}` });
                        setisopen(false)
                    }}>
                        <View className='body' style={{ color: "#6190E8" }}>
                            交接维修
                        </View>
                    </AtActionSheetItem>
                }
                {
                    actions == "action" && <AtActionSheetItem onClick={() => {
                        Taro.navigateTo({ url: `/pages/actionform/index?type=repair_outside&id=${id}` });
                        setisopen(false)
                    }}>
                        <View className='body' style={{ color: "#6190E8" }}>
                            申请外协
                        </View>
                    </AtActionSheetItem>
                }

                {
                    actions == "action" && <AtActionSheetItem onClick={() => {
                        Taro.navigateTo({ url: `/pages/actionform/index?type=repair_return&id=${id}` });
                        setisopen(false)
                    }}>
                        <View className='body' style={{ color: "#6190E8" }}>
                            退回预判
                        </View>
                    </AtActionSheetItem>
                }

                {
                    actions == "action" && <AtActionSheetItem onClick={() => {
                        Taro.navigateTo({ url: `/pages/actionform/index?type=repair_back&id=${id}` });
                        setisopen(false)
                    }}>
                        <View className='body' style={{ color: "#6190E8" }}>
                            退单
                        </View>
                    </AtActionSheetItem>
                }


                <AtActionSheetItem onClick={() => {
                    setisopen(false)
                }}>
                    <View className='body'>
                        取消
                    </View>
                </AtActionSheetItem>
            </AtActionSheet>
            <AtTabs
                current={current}
                scroll
                tabList={type == "repair" ? [
                    { title: '故障信息' },
                    { title: '预判信息' },
                    { title: '执行信息' }
                ] : [
                    { title: '故障信息' },
                    { title: '执行信息' }
                ]}
                onClick={(val) => {
                    setcurrent(val)
                }}>
                <AtTabsPane current={current} index={0}>
                    {renderFirst}
                </AtTabsPane>
                {
                    type == "repair" && <AtTabsPane current={current} index={1}>
                        <RSecond res={res}></RSecond>
                    </AtTabsPane>
                }

                {
                    type == "repair" && <AtTabsPane current={current} index={2}>
                        <RThird res={res}></RThird>
                    </AtTabsPane>
                }

                {
                    type == "trace" && <AtTabsPane current={current} index={1}>
                        <TSecond res={res}></TSecond>
                    </AtTabsPane>
                }

                {
                    type == "help" && <AtTabsPane current={current} index={1}>
                        <HSecond res={res}></HSecond>
                    </AtTabsPane>
                }
            </AtTabs>
            <View className='center' style={{ height: "48PX", width: "100%" }} />

            {
                (ifsd && !action) &&
                <View className='center' style={{ position: "fixed", bottom: 0, height: "48PX", width: "100%", backgroundColor: "#f0f0f0" }}>
                    <View className='center' style={{ flex: 1, color: "#6190E8", fontSize: "16PX" }} onClick={() => {
                        Taro.navigateTo({ url: `/pages/actionform/index?type=repair_updo&id=${id}&applyRepairId=${res?.taskInfo?.applyRepairId}&equipmentId=${res?.baseInfo?.equipmentId}&shopId=${res?.baseInfo?.shopId}&sectionId=${sectionId}` })
                    }}>
                        预判
                    </View>
                    <View className='center' style={{ flex: 1, color: "red", fontSize: "16PX" }} onClick={() => {
                        setisopen(true);
                        setaction("repair_delete");
                    }}>
                        删除
                    </View>
                </View>
            }

            {
                (ifsa && !action) &&
                <View className='center' style={{ position: "fixed", bottom: 0, height: "48PX", width: "100%", backgroundColor: "#f0f0f0" }}>
                    <View className='center' style={{ flex: 1, color: "#6190E8", fontSize: "16PX" }} onClick={() => {
                        Taro.navigateTo({ url: `/pages/actionform/index?type=repair_pad&id=${id}&applyRepairId=${res?.taskInfo?.applyRepairId}&shopId=${res?.baseInfo?.shopId}&workingHours=${res?.predictionInfo?.workingHours}` })
                    }}>
                        派单
                    </View>
                </View>
            }

            {
                (ifsb && !action) && <View className='center' style={{ position: "fixed", bottom: 0, height: "48PX", width: "100%", backgroundColor: "#f0f0f0" }}>
                    <View className='center' style={{ flex: 1, color: "#6190E8", fontSize: "16PX" }} onClick={() => {
                        let finishRepairTime = dayjs().format("YYYY-MM-DD HH:mm");
                        Taro.navigateTo({ url: `/pages/actionform/index?type=repair_finish&id=${id}&equipmentId=${res?.baseInfo?.equipmentId}&equipmentFaultId=${res?.faultInfo?.equipmentFaultId}&equipmentFaultDetailId=${res?.faultInfo?.equipmentFaultDetailId}&acceptRepairTime=${res?.repairInfo?.acceptRepairTime}&finishRepairTime=${finishRepairTime}` })
                    }}>
                        完成维修
                    </View>
                    <View className='center' style={{ flex: 1, color: "#6190E8", fontSize: "16PX" }} onClick={() => {
                        setisopen(true);
                        setaction("action");
                    }}>
                        更多
                    </View>
                </View>
            }

            {
                (ifsc && !action) && <View className='center' style={{ position: "fixed", bottom: 0, height: "48PX", width: "100%", backgroundColor: "#f0f0f0" }}>
                    <View className='center' style={{ flex: 1, color: "#6190E8", fontSize: "16PX" }} onClick={() => {
                        Taro.navigateTo({ url: `/pages/actionform/index?type=repair_confirm&id=${id}` })
                    }}>
                        验证
                    </View>
                </View>
            }
            {
                (ifse && !action) && <View className='center' style={{ position: "fixed", bottom: 0, height: "48PX", width: "100%", backgroundColor: "#f0f0f0" }}>
                    <View className='center' style={{ flex: 1, color: "#6190E8", fontSize: "16PX" }} onClick={() => {
                        Taro.navigateTo({ url: `/pages/actionform/index?type=repair_confirm2&id=${id}&confirmIsPass=${res?.confirmInfo?.confirmIsPass}&confirmDesc=${res?.confirmInfo?.confirmDesc}` })
                    }}>
                        确认
                    </View>
                </View>
            }
            {
                (zzifs && !action && type == "trace") && <View className='center' style={{ position: "fixed", bottom: 0, height: "48PX", width: "100%", backgroundColor: "#f0f0f0" }}>
                    <View className='center' style={{ flex: 1, color: "#6190E8", fontSize: "16PX" }} onClick={() => {
                        setisopen(true);
                        setaction("accept");
                    }}>
                        接单
                    </View>
                    <View className='center' style={{ flex: 1, color: "red", fontSize: "16PX" }} onClick={() => {
                        setisopen(true);
                        setaction("repair_delete");
                    }}>
                        删除
                    </View>
                </View>
            }

            {
                (zzifsc && !action && type == "trace") && <View className='center' style={{ position: "fixed", bottom: 0, height: "48PX", width: "100%", backgroundColor: "#f0f0f0" }}>
                    <View className='center' style={{ flex: 1, color: "#6190E8", fontSize: "16PX" }} onClick={() => {
                        Taro.navigateTo({ url: `/pages/actionform/index?type=repair_progress&id=${id}` })
                    }}>
                        进度更新
                    </View>
                </View>
            }

            {
                (zzifss && !action && type == "trace") && <View className='center' style={{ position: "fixed", bottom: 0, height: "48PX", width: "100%", backgroundColor: "#f0f0f0" }}>
                    <View className='center' style={{ flex: 1, color: "#6190E8", fontSize: "16PX" }} onClick={() => {
                        Taro.navigateTo({ url: `/pages/actionform/index?type=repair_docheck&id=${id}` })
                    }}>
                        验证
                    </View>
                </View>
            }


            {
                (wxifs && !action && type == "help") && <View className='center' style={{ position: "fixed", bottom: 0, height: "48PX", width: "100%", backgroundColor: "#f0f0f0" }}>
                    <View className='center' style={{ flex: 1, color: "#6190E8", fontSize: "16PX" }} onClick={() => {
                        setisopen(true);
                        setaction("helpaccept");
                    }}>
                        接单
                    </View>
                    <View className='center' style={{ flex: 1, color: "red", fontSize: "16PX" }} onClick={() => {
                        setisopen(true);
                        setaction("repair_delete");
                    }}>
                        删除
                    </View>
                </View>
            }

            {
                (wxifsc && !action && type == "help") && <View className='center' style={{ position: "fixed", bottom: 0, height: "48PX", width: "100%", backgroundColor: "#f0f0f0" }}>
                    <View className='center' style={{ flex: 1, color: "#6190E8", fontSize: "16PX" }} onClick={() => {
                        Taro.navigateTo({ url: `/pages/actionform/index?type=finish_outside&id=${id}` })
                    }}>
                        完成外协
                    </View>
                </View>
            }
        </View>

    )
};

export default Repairdetail;
