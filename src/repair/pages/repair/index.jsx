import React from 'react';
import { Text, View } from '@tarojs/components';
import InitForm from '../../../components/InitForm';
import { useState } from 'react';
import { devicelist, destorytype, destoryname, destorydesc } from '../../../utils/service';
import { request } from '../../../utils/request';
import { useRequest } from 'ahooks';
import Taro from '@tarojs/taro';
import dayjs from 'dayjs';



const Dorepair = ({
    params,
}) => {
    const { equipmentId, disabled } = Taro.Current.router.params;

    let [fields, cfields] = useState({
        "orderType": {
            "value": disabled ? "2" : "1",
            "type": "select",
            "title": "工单类型",
            "name": [
                "orderType"
            ],
            "required": true,
            "linked": ["equipmentId", "equipmentFaultId", "equipmentFaultDetailId"],
            "options": [
                {
                    "label": "维修工单",
                    "value": "1"
                },
                {
                    "label": "追踪工单",
                    "value": "2"
                },
                {
                    "label": "外协工单",
                    "value": "3"
                },
            ]
        },
        "equipmentId": {
            "value": equipmentId ?? null,
            "type": "select",
            "title": "设备",
            "name": [
                "equipmentId"
            ],
            "required": true,
            "belinked": {
                "options": {
                    "database": devicelist,
                    "params": {
                        "orderType": "linked"
                    }
                }
            },
            "linked": ["equipmentFaultId", "equipmentFaultDetailId"]
        },
        // "faultOccurDate": {
        //     "value": dayjs().format("YYYY-MM-DD"),
        //     "type": "datepicker",
        //     "title": "故障发生日期",
        //     "name": [
        //         "faultOccurDate"
        //     ],
        //     "required": true,
        // },
        // "faultOccurTime": {
        //     "value": dayjs().format("HH:mm"),
        //     "type": "datepicker",
        //     "title": "故障发生时间",
        //     "name": [
        //         "faultOccurTime"
        //     ],
        //     "required": true,
        //     "showTime": true,
        // },
        "isStop": {
            "value": "0",
            "type": "select",
            "title": "是否停机",
            "name": [
                "isStop"
            ],
            "required": true,
            "options": [
                {
                    "label": "未停机",
                    "value": "0"
                },
                {
                    "label": "已停机",
                    "value": "1"
                }
            ]
        },
        "equipmentFaultId": {
            "value": null,
            "type": "select",
            "title": "故障分类",
            "name": [
                "equipmentFaultId"
            ],
            "required": false,
            "linked": ["equipmentFaultDetailId"],
            "belinked": {
                "options": {
                    "database": destorytype,
                    "params": {
                        "equipmentId": "linked"
                    }
                }
            }

        },
        "equipmentFaultDetailId": {
            "value": null,
            "type": "select",
            "title": "故障名称",
            "name": [
                "equipmentFaultDetailId"
            ],
            "required": false,
            "belinked": {
                "options": {
                    "database": destoryname,
                    "params": {
                        "equipmentFaultId": "linked"
                    }
                }
            },
            "update": {
                effectItems: ["faultDesc"],
                database: destorydesc,
                paramskey: "faultDetailId",
            },
        },
        "faultDesc": {
            "value": null,
            "type": "input",
            "title": "故障描述",
            "name": [
                "faultDesc"
            ],
            "required": false
        },
        "urlIds": {
            "value": null,
            "type": "upload",
            "title": "故障图片",
            "name": [
                "urlIds"
            ],
            "required": false,
            "listType": "img",
            "multiple": true,
            "limit": 5
        }
    })
    let { run, loading } = useRequest(request, {
        debounceWait: 300, //防抖
        manual: true,
        formatResult: (res) => res,
        onSuccess: (result, params) => {
            if (result?.data?.code == "0000") {
                Taro.showToast({
                    title: "操作成功",
                    icon: 'success',
                    duration: 2000
                });
                Taro.navigateBack();
            }

        },
    });


    return (
        <View style={{ padding: 12 }}>
            <InitForm
                fields={fields}
                loading={loading}
                submit={(data, reset) => {
                    let newdata = {
                        ...data,
                        // faultOccurTime: data?.faultOccurDate + " " + data?.faultOccurTime
                    }

                    delete newdata.faultOccurDate;
                    run("/ngic-app/repair/applyRepair", { ...newdata });
                }}
            >

            </InitForm>
        </View>
    )
};

export default Dorepair;
