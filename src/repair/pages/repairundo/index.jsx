import React, { useState } from 'react';
import { View, Input } from '@tarojs/components';
import AutoList from '../../../components/AutoList';
import Taro, { useDidShow } from '@tarojs/taro';
import { AtTabs, AtTabsPane, AtIcon } from 'taro-ui'
import "taro-ui/dist/style/components/tabs.scss" // 按需引入
import "taro-ui/dist/style/components/icon.scss"; // 按需引入



const Maintain = ({
    params,
}) => {
    const [refresh, setrefresh] = useState();
    const [current, setcurrent] = useState(0);

    //操作返回刷新
    useDidShow(() => {
        let pages = Taro.getCurrentPages();
        let currPage = pages[pages.length - 1];
        const { refresh } = currPage;
        if (refresh) {
            setrefresh(true);
            setrefresh(false);
        }
    })

    let renderFirst = (
        <AutoList
            refresh={refresh}
            path="/ngic-app/repair/repair/queryAllList"
            height={"calc(100vh - 106PX)"}
            row={(item, i) => {
                return <View className='item' onClick={() => {
                    Taro.navigateTo({ url: `/repair/pages/repairdetail/index?taskNo=${item.taskNo}&type=repair&id=${item.id}&action=none` })
                }}>
                    <View style={{ display: "flex", justifyContent: "space-between", alignItems: "center", borderBottom: "1PX solid #f0f0f0", padding: "0PX 0 10PX 0", marginBottom: 10 }}>
                        <View style={{ flex: 1, overflow: "hidden", textOverflow: "ellipsis", whiteSpace: "nowrap" }}>工单:{item.taskNo}</View>
                        <View className={`status_${item.status}`}>
                            {item.repairStatusName}
                        </View>
                    </View>
                    <View className='center' style={{ flexDirection: "column", width: "100%", fontSize: 15 }}>
                        <View className='center' style={{ justifyContent: "space-between", width: "100%" }}>
                            <View>
                                {item.equipmentName}
                            </View>
                            <View style={{ marginLeft: 12 }}>
                                {item.equipmentNo}
                            </View>
                        </View>
                        <View className='center' style={{ justifyContent: "space-between", width: "100%", marginTop: 6,alignItems:"flex-start" }}>
                            <View style={{flex:1}}>
                                工厂：{item.shopName}
                            </View>
                            <View style={{ flex:1,marginLeft: 12,display:"flex",justifyContent:"flex-end" }}>
                                车间：{item.sectionName}
                            </View>
                        </View>
                        <View className='center' style={{ justifyContent: "flex-end", width: "100%", marginTop: 6 }}>
                            <AtIcon value='clock' size='16' color='#999'></AtIcon>
                            <View style={{ marginLeft: 6, color: "#999" }}>
                                {item.applyRepairTime}
                            </View>
                        </View>

                    </View>
                </View>
            }}
        />
    ), renderSecond = (
        <AutoList
            refresh={refresh}
            path="/ngic-app/repair/trace/queryAllList"
            height={"calc(100vh - 106PX)"}
            row={(item, i) => {
                return <View className='item' onClick={() => {
                    Taro.navigateTo({ url: `/repair/pages/repairdetail/index?taskNo=${item.taskNo}&type=trace&id=${item.id}&action=none` })
                }}>
                    <View style={{ display: "flex", justifyContent: "space-between", alignItems: "center", borderBottom: "1PX solid #f0f0f0", padding: "0PX 0 10PX 0", marginBottom: 10 }}>
                        <View style={{ flex: 1, overflow: "hidden", textOverflow: "ellipsis", whiteSpace: "nowrap" }}>工单:{item.taskNo}</View>
                        <View className={`status_${item.traceStatus}`}>{item.traceStatusName}</View>
                    </View>
                    <View className='center' style={{ flexDirection: "column", width: "100%", fontSize: 15 }}>
                        <View className='center' style={{ justifyContent: "space-between", width: "100%" }}>
                            <View>
                                {item.equipmentName}
                            </View>
                            <View style={{ marginLeft: 12 }}>
                                {item.equipmentNo}
                            </View>
                        </View>
                        <View className='center' style={{ justifyContent: "space-between", width: "100%", marginTop: 6,alignItems:"flex-start" }}>
                            <View style={{flex:1}}>
                                工厂：{item.shopName}
                            </View>
                            <View style={{ flex:1,marginLeft: 12,display:"flex",justifyContent:"flex-end" }}>
                                车间：{item.sectionName}
                            </View>
                        </View>
                        <View className='center' style={{ justifyContent: "space-between", width: "100%", marginTop: 6 }}>
                            <View>
                                计划日期：
                                {item.planCheckDate}
                            </View>
                            <View className='center'>
                                <AtIcon value='clock' size='16' color='#999'></AtIcon>
                                <View style={{ marginLeft: 6, color: "#999" }}>
                                    {item.applyRepairTime}
                                </View>
                            </View>

                        </View>

                    </View>
                </View>
            }}
        />
    ), renderThird = (
        <AutoList
            refresh={refresh}
            path="/ngic-app/repair/help/queryAllList"
            height={"calc(100vh - 106PX)"}
            row={(item, i) => {
                return <View className='item' onClick={() => {
                    Taro.navigateTo({ url: `/repair/pages/repairdetail/index?taskNo=${item.taskNo}&type=help&id=${item.id}&action=none` })
                }}>
                    <View style={{ display: "flex", justifyContent: "space-between", alignItems: "center", borderBottom: "1PX solid #f0f0f0", padding: "0PX 0 10PX 0", marginBottom: 10 }}>
                        <View style={{ flex: 1, overflow: "hidden", textOverflow: "ellipsis", whiteSpace: "nowrap" }}>工单:{item.taskNo}</View>
                        <View className={`status_${item.status}`}>{item.statusName}</View>
                    </View>
                    <View className='center' style={{ flexDirection: "column", width: "100%", fontSize: 15 }}>
                        <View className='center' style={{ justifyContent: "space-between", width: "100%" }}>
                            <View>
                                {item.equipmentName}
                            </View>
                            <View style={{ marginLeft: 12 }}>
                                {item.equipmentNo}
                            </View>
                        </View>
                        <View className='center' style={{ justifyContent: "space-between", width: "100%", marginTop: 6,alignItems:"flex-start" }}>
                            <View style={{flex:1}}>
                                工厂：{item.shopName}
                            </View>
                            <View style={{ flex:1,marginLeft: 12,display:"flex",justifyContent:"flex-end" }}>
                                车间：{item.sectionName}
                            </View>
                        </View>
                        <View className='center' style={{ justifyContent: "flex-end", width: "100%", marginTop: 6 }}>
                            <AtIcon value='clock' size='16' color='#999'></AtIcon>
                            <View style={{ marginLeft: 6, color: "#999" }}>
                                {item.applyRepairTime}
                            </View>
                        </View>
                    </View>
                </View>
            }}
        />
    )



    return (
        <View >
            <AtTabs
                current={current}
                scroll
                tabList={[
                    { title: '维修工单' },
                    { title: '追踪工单' },
                    { title: '外协工单' }
                ]}
                onClick={(val) => {
                    setcurrent(val)
                }}>
                <AtTabsPane current={current} index={0}>
                    {renderFirst}
                </AtTabsPane>
                <AtTabsPane current={current} index={1}>
                    {renderSecond}
                </AtTabsPane>
                <AtTabsPane current={current} index={2}>
                    {renderThird}
                </AtTabsPane>
            </AtTabs>

        </View >
    )
};

export default Maintain;
