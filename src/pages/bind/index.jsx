import { useState, useEffect } from 'react';
import { View, Text, Input, } from '@tarojs/components';
import { AtIcon, AtButton, AtMessage } from 'taro-ui';
import Taro from '@tarojs/taro';
import "taro-ui/dist/style/components/button.scss"; // 按需引入
import "taro-ui/dist/style/components/icon.scss"; // 按需引入
import "taro-ui/dist/style/components/message.scss"; // 按需引入

import './index.less';
import { request } from '../../utils/request';


function Bind() {
  const [val, setval] = useState();
  useEffect(() => {

  }, [])

  return (
    <View className='center'>
      <AtMessage />
      <Text style={{ marginTop: 36, marginBottom: 12, fontSize: 14 }}>请输入您在管理平台录入的个人手机号</Text>
      <View style={{ background: "#f0f0f0", width: "80%", height: 36, borderRadius: 6, marginTop: 12, display: "flex", justifyContent: "center", alignItems: "center" }}>
        <View style={{ margin: "0 6PX 4PX 12PX" }}>
          <AtIcon value='phone' size='24' color='#F00'></AtIcon>
        </View>

        <Input value={val} onInput={(e) => {
          setval(e.target.value)
        }} style={{ flex: 1, fontSize: 16 }} type='text' placeholder='请输入' focus
        />
      </View>
      <View style={{ marginTop: 48, width: "80%" }}>
        <AtButton type='primary' onClick={() => {
          let userid = Taro.getStorageSync('userkey')
          request("/ngic-app/wechatwork/identify/bind", {
            "wechatworkId": userid,
            "telephone": val
          }, (res) => {
            if (res.code != "0000") {
              Taro.atMessage({
                'message': res.msg,
                'type': "error",
              })
            } else {
              Taro.setStorageSync("userinfo", JSON.stringify(res?.data));
              Taro.atMessage({
                'message': "绑定成功",
                'type': "success",
              })
              setTimeout(() => {
                Taro.switchTab({ url: "/pages/index/index" })
              }, 1000)
            }
          })
        }}
        >绑定</AtButton>
      </View>


    </View>
  )
}

export default Bind;
