import { useState, useEffect } from 'react';
import { View} from '@tarojs/components';
import Taro from '@tarojs/taro';
import "taro-ui/dist/style/components/list.scss"; // 按需引入
import "taro-ui/dist/style/components/icon.scss"; // 按需引入
import "taro-ui/dist/style/components/toast.scss"; // 按需引入
import './index.less';
import Detail from '../../components/Detail';




function Center() {
  const [info, setinfo] = useState();
  useEffect(() => {
    let infos = Taro.getStorageSync("userinfo");
    if (infos) {
      infos = JSON.parse(infos);
    }
    setinfo(infos)
  }, [])

  return (
    <View>

      <Detail
        hasSplit={true}
        columns={[
          {
            "title": "姓名",
            "dataIndex": "userName",
            "key": "userName"
          },
          {
            "title": "性别",
            "dataIndex": "gender",
            "key": "gender"
          },
          {
            "title": "用户名",
            "dataIndex": "accountName",
            "key": "accountName"
          },
          {
            "title": "联系电话",
            "dataIndex": "telephone",
            "key": "telephone"
          },
          {
            "title": "邮箱",
            "dataIndex": "mailNo",
            "key": "mailNo"
          }
        ]}
        dataSource={info?.user ?? {}}
      >
      </Detail>
      <Detail
        hasSplit={true}
        columns={[
          {
            "title": "组织名称",
            "dataIndex": "factoryName",
            "key": "factoryName"
          },
          {
            "title": "部门名称",
            "dataIndex": "departmentName",
            "key": "departmentName"
          },
          {
            "title": "负责工厂",
            "dataIndex": "shopNames",
            "key": "shopNames"
          },
          {
            "title": "负责车间",
            "dataIndex": "sectionNames",
            "key": "sectionNames"
          },
          {
            "title": "角色名称",
            "dataIndex": "roleNames",
            "key": "roleNames"
          },
          {
            "title": "直属领导",
            "dataIndex": "parentName",
            "key": "parentName"
          }
        ]}
        dataSource={info?.user ?? {}}
      >
      </Detail>

      <Detail
        hasSplit={true}
        columns={[
          {
            "title": "学历",
            "dataIndex": "academicCareer",
            "key": "academicCareer"
          },
          {
            "title": "毕业院校",
            "dataIndex": "university",
            "key": "university"
          },
          {
            "title": "专业",
            "dataIndex": "major",
            "key": "major"
          },
          {
            "title": "备注",
            "dataIndex": "remarks",
            "key": "remarks"
          }
        ]}
        dataSource={info?.user ?? {}}
      >
      </Detail>

    </View>
  )
}

export default Center;
