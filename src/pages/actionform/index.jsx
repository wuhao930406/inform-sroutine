import Taro from '@tarojs/taro';
import { useAsyncEffect } from 'ahooks';
import { usershop, usershops, destorytype, repairtype, destoryname, destorydesc } from '../../utils/service';
import React, { useMemo, useState } from 'react';
import InitForm from '../../components/InitForm';
import { request } from '../../utils/request';
let titleaction = {
    repair_updo: "维修预判",
    repair_pad: "维修派单",
    repair_finish: "完成维修",
    repair_applyhelp: "申请援助",
    repair_hand: "交接维修",
    repair_outside: "申请外协",
    repair_return: "退回预判",
    repair_back: "退单",
    repair_confirm: "效果确认",
    repair_confirm2: "确认",
    repair_progress: "进度更新",
    repair_docheck: "验证",
    finish_outside: "完成外协",
    maintain_delay: "延期保养",
    maintain_finish: "完成保养",
    maintain_audit: "延期审批",
    maintain_approval: "验证",
    inspection_delay: "延期点检",
    inspection_finish: "完成点检",
    inspection_audit: "延期审批"
}

function ActionForm() {
    const { type, id, applyRepairId, shopId, workingHours, equipmentId, equipmentFaultId, equipmentFaultDetailId,
        sectionId, acceptRepairTime, finishRepairTime, confirmDesc, confirmIsPass
    } = Taro.Current.router.params;
    const [loading, setloading] = useState(false);
    const [table, settable] = useState([]);
    const [datas, setdatas] = useState();
    //返回上个页面刷新
    function back() {
        if (["maintain_approval", "inspection_finish", "inspection_audit"].indexOf(type) != -1) {
            var pages = Taro.getCurrentPages();
            var prevPage = pages[pages.length - 3]; //上2个页面
            prevPage.refresh = true;
            Taro.navigateBack({
                delta: 2
            });
            return
        }

        Taro.navigateBack();
    }


    //统一提交处理
    let dosubmit = (data) => {
        setloading(true);
        let extraparams = type == "repair_updo" ? {
            applyRepairId,
        } : {
            id
        }

        if (type == "repair_finish") {
            let newdata = {
                ...data,
                startRepairTime: acceptRepairTime,
                finishRepairTime
            }
            data = newdata;
        }
        if (type == "repair_confirm2") {
            let newarr = data?.assistUserList?.map(it => {
                return {
                    id: it.id,
                    actualWorkHours2: it.actualWorkHours
                }
            }) ?? [];
            data.assistUserList = newarr;
        }
        request(fields[type].path, { ...data, ...extraparams }, (res) => {
            if (res.code == "0000") {
                Taro.showToast({
                    title: "操作成功",
                    icon: "success",
                    duration: 2000
                })
                setTimeout(() => {
                    setloading(false);
                    back();
                }, 1000)
            }
        })

        setTimeout(() => {
            setloading(false)
        }, 3000)
    }

    //初始化数据
    useAsyncEffect(async () => {
        Taro.setNavigationBarTitle({
            title: titleaction[type]
        })
        if (type == "maintain_finish" || type == "maintain_delay") {
            let res = await request("/ngic-app/app/equipmentMaintain/queryDetail", { id })
            settable(res?.data?.data?.itemInfo?.item ?? [])
            setdatas(res?.data?.data)
        }

        if (type == "inspection_finish" || type == "inspection_delay") {
            let res = await request("/ngic-app/wechatwork/equipmentCheck/queryDetail", { id })
            settable(res?.data?.data?.checkItem?.item ?? [])
            setdatas(res?.data?.data)
        }
        if (type == "repair_confirm" || type == "repair_confirm2") {
            let res = await request("/ngic-app/repair/repair/assistuser", { id });
            settable(res?.data?.data?.dataList ?? [])
        }

    }, []);
    let fields = useMemo(() => {
        return {
            //维修预判
            repair_updo: {
                fields: {
                    "workingHours": {
                        "value": null,
                        "type": "input",
                        "title": "预计工时",
                        "name": [
                            "workingHours"
                        ],
                        "required": true
                    },
                    "repairScheme": {
                        "value": null,
                        "type": "input",
                        "title": "方案",
                        "name": [
                            "repairScheme"
                        ],
                        "required": true
                    },
                    "consumeList": {
                        "value": null,
                        "type": "jumpselect",
                        "title": "非寿命件更换",
                        "path": "/ngic-app/app/spareParts/queryWmsStock",
                        "params": {
                            shopId,
                            sectionId
                        },
                        "name": [
                            "consumeList"
                        ],
                        "required": false,
                        "place": "num"
                    },
                    "replaceList": {
                        "value": null,
                        "type": "jumpselect",
                        "title": "寿命件更换",
                        "path": "/ngic-app/app/spareParts/querySpareSupplierWms",
                        "params": {
                            equipmentId
                        },
                        "name": [
                            "replaceList"
                        ],
                        "required": false,
                        "place": "num"
                    }
                },
                path: "/ngic-app/repair/repair/repairPrediction"
            },
            //维修派单
            repair_pad: {
                fields: {
                    "workingHoures": {
                        "value": workingHours,
                        "type": "input",
                        "title": "预计工时",
                        "name": [
                            "workingHoures"
                        ],
                        "required": false,
                        "disabled": true
                    },
                    "repairUserId": {
                        "value": null,
                        "type": "select",
                        "title": "维修人员",
                        "name": [
                            "repairUserId"
                        ],
                        "required": true,
                        "options": {
                            database: usershops,
                            params: {
                                id: id
                            }
                        }
                    },
                    "workingHours": {
                        "value": null,
                        "type": "input",
                        "title": "工时",
                        "name": [
                            "workingHours"
                        ],
                        "required": true
                    },
                    "assistUserList": {
                        "value": [],
                        "type": "addtable",
                        "title": "协助人员",
                        "name": [
                            "assistUserList"
                        ],
                        "maxlength": 5,
                        "columns": [
                            {
                                "title": "协助人",
                                "dataIndex": "repairUserId",
                                "key": "repairUserId",
                                "type": "select",
                                "options": {
                                    database: usershop,
                                    params: {
                                        shopId: shopId
                                    }
                                }
                            },
                            {
                                "title": "工时",
                                "dataIndex": "workingHours",
                                "key": "workingHours",
                                "type": "input"
                            }
                        ],
                        "required": false
                    }
                },
                path: "/ngic-app/repair/repair/accept"
            },
            //完成维修
            repair_finish: {
                fields: {
                    "equipmentFaultId": {
                        "value": equipmentFaultId,
                        "type": "select",
                        "title": "故障分类",
                        "name": [
                            "equipmentFaultId"
                        ],
                        "required": true,
                        "linked": ["equipmentFaultDetailId"],
                        "options": {
                            "database": destorytype,
                            "params": {
                                "equipmentId": equipmentId
                            }
                        }

                    },
                    "equipmentFaultDetailId": {
                        "value": equipmentFaultDetailId,
                        "type": "select",
                        "title": "故障名称",
                        "name": [
                            "equipmentFaultDetailId"
                        ],
                        "required": true,
                        "belinked": {
                            "options": {
                                "database": destoryname,
                                "params": {
                                    "equipmentFaultId": "linked"
                                }
                            }
                        },
                        "update": {
                            effectItems: ["repairContent"],
                            database: destorydesc,
                            paramskey: "faultDetailId",
                        },
                    },
                    "faultReason": {
                        "value": null,
                        "type": "input",
                        "title": "故障原因",
                        "name": [
                            "faultReason"
                        ],
                        "required": false
                    },
                    "isStop": {
                        "value": "1",
                        "type": "select",
                        "title": "是否停机",
                        "name": [
                            "isStop"
                        ],
                        "required": true,
                        "options": [
                            {
                                "label": "未停机",
                                "value": "0"
                            },
                            {
                                "label": "已停机",
                                "value": "1"
                            }
                        ]
                    },
                    // "startRepairDate": {
                    //     "value": null,
                    //     "type": "datepicker",
                    //     "title": "开始维修日期",
                    //     "name": [
                    //         "startRepairDate"
                    //     ],
                    //     "required": true,
                    //     "format": "YYYY-MM-DD"
                    // },
                    // "startRepairTime": {
                    //     "value": null,
                    //     "type": "datepicker",
                    //     "title": "开始维修时间",
                    //     "name": [
                    //         "startRepairTime"
                    //     ],
                    //     "required": true,
                    //     "format": "HH:mm",
                    //     "showTime": true,
                    // },
                    // "finishRepairDate": {
                    //     "value": null,
                    //     "type": "datepicker",
                    //     "title": "完成维修日期",
                    //     "name": [
                    //         "finishRepairDate"
                    //     ],
                    //     "required": true,
                    //     "format": "YYYY-MM-DD"
                    // },
                    // "finishRepairTime": {
                    //     "value": null,
                    //     "type": "datepicker",
                    //     "title": "完成维修时间",
                    //     "name": [
                    //         "finishRepairTime"
                    //     ],
                    //     "required": true,
                    //     "format": "HH:mm",
                    //     "showTime": true,
                    // },
                    "repairContent": {
                        "value": null,
                        "type": "textarea",
                        "title": "维修内容",
                        "name": [
                            "repairContent"
                        ],
                        "required": true
                    },
                    "repairType": {
                        "value": null,
                        "type": "select",
                        "title": "维修类型",
                        "name": [
                            "repairType"
                        ],
                        "required": false,
                        "options": {
                            "database": repairtype,
                            "params": {

                            }
                        }
                    }
                },
                path: "/ngic-app/repair/repair/repairEnd"
            },
            //申请协助
            repair_applyhelp: {
                fields: {
                    "applyAssistReason": {
                        "value": null,
                        "type": "textarea",
                        "title": "申请协助原因",
                        "name": [
                            "applyAssistReason"
                        ],
                        "required": true
                    }
                },
                path: "/ngic-app/repair/repair/applyHelp"
            },
            //交接维修
            repair_hand: {
                fields: {
                    "handoverContent": {
                        "value": null,
                        "type": "textarea",
                        "title": "交接维修原因",
                        "name": [
                            "handoverContent"
                        ],
                        "required": true
                    }
                },
                path: "/ngic-app/repair/repair/handRepair"
            },
            //申请外协
            repair_outside: {
                fields: {
                    "applyOuthelpReason": {
                        "value": null,
                        "type": "textarea",
                        "title": "申请外协内容",
                        "name": [
                            "applyOuthelpReason"
                        ],
                        "required": true
                    }
                },
                path: "/ngic-app/repair/repair/applyOutHelp"
            },
            //退回预判
            repair_return: {
                fields: {
                    "returnPredictionReason": {
                        "value": null,
                        "type": "textarea",
                        "title": "退回预判原因",
                        "name": [
                            "returnPredictionReason"
                        ],
                        "required": true
                    }
                },
                path: "/ngic-app/repair/repair/returnPrediction"
            },
            //退回
            repair_back: {
                fields: {
                    "returnOrderReason": {
                        "value": null,
                        "type": "textarea",
                        "title": "退单原因",
                        "name": [
                            "returnOrderReason"
                        ],
                        "required": true
                    }
                },
                path: "/ngic-app/repair/repair/returnOrder"
            },
            //效果确认
            repair_confirm: {
                fields: {
                    "confirmIsPass": {
                        "value": null,
                        "type": "select",
                        "title": "审批意见",
                        "name": [
                            "confirmIsPass"
                        ],
                        "required": true,
                        "options": [
                            {
                                "label": "同意",
                                "value": "1"
                            },
                            {
                                "label": "拒绝",
                                "value": "2"
                            },
                            {
                                "label": "带病生产",
                                "value": "3"
                            }
                        ]
                    },
                    "assistUserList": {
                        "value": null,
                        "title": "确认工时",
                        "type": "listsc",
                        "name": ['assistUserList'],
                        "required": true,
                        "hides": table.length == 0,
                        "dataSource": table,
                        "columns": [
                            {
                                "title": '维修人员',
                                "dataIndex": 'repairUserName',
                                "key": 'repairUserName'
                            },
                            {
                                "title": '维修工时',
                                "dataIndex": 'workingHours',
                                "key": 'workingHours'
                            },
                            {
                                "title": '实际工时',
                                "dataIndex": 'actualWorkHours',
                                "key": 'actualWorkHours',
                                "submittype": "inputnumber"
                            }
                        ],
                        "options": [
                            {
                                "label": "",
                                "value": ""
                            }
                        ],
                        "multiple": true
                    },
                    "confirmDesc": {
                        "value": null,
                        "type": "textarea",
                        "title": "审批内容",
                        "name": [
                            "confirmDesc"
                        ],
                        "required": true
                    },
                    // "confirmFileIdList": {
                    //     "value": null,
                    //     "type": "upload",
                    //     "title": "上传文件",
                    //     "name": [
                    //         "confirmFileIdList"
                    //     ],
                    //     "required": true,
                    //     "listType": "img",
                    //     "multiple": true,
                    //     "limit": 5
                    // }
                },
                path: "/ngic-app/repair/repair/confirm"
            },
            //确认
            repair_confirm2: {
                fields: {
                    "confirm2IsPass": {
                        "value": confirmIsPass,
                        "type": "select",
                        "title": "审批意见",
                        "name": [
                            "confirm2IsPass"
                        ],
                        "required": true,
                        "options": [
                            {
                                "label": "同意",
                                "value": "1"
                            },
                            {
                                "label": "拒绝",
                                "value": "2"
                            },
                            {
                                "label": "带病生产",
                                "value": "3"
                            }
                        ]
                    },
                    "assistUserList": {
                        "value": table,
                        "title": "确认工时",
                        "type": "listsc",
                        "name": ['assistUserList'],
                        "required": true,
                        "hides": table.length == 0,
                        "dataSource": table,
                        "columns": [
                            {
                                "title": '维修人员',
                                "dataIndex": 'repairUserName',
                                "key": 'repairUserName'
                            },
                            {
                                "title": '维修工时',
                                "dataIndex": 'workingHours',
                                "key": 'workingHours'
                            },
                            {
                                "title": '实际工时',
                                "dataIndex": 'actualWorkHours',
                                "key": 'actualWorkHours',
                                "submittype": "inputnumber"
                            }
                        ],
                        "options": [
                            {
                                "label": "",
                                "value": ""
                            }
                        ],
                        "multiple": true
                    },
                    "confirm2Desc": {
                        "value": confirmDesc,
                        "type": "textarea",
                        "title": "审批内容",
                        "name": [
                            "confirm2Desc"
                        ],
                        "required": true
                    },
                    // "confirm2FileIdList": {
                    //     "value": null,
                    //     "type": "upload",
                    //     "title": "上传文件",
                    //     "name": [
                    //         "confirm2FileIdList"
                    //     ],
                    //     "required": true,
                    //     "listType": "img",
                    //     "multiple": true,
                    //     "limit": 5
                    // }
                },
                path: "/ngic-app/repair/repair/confirm2"
            },
            //追踪单进度更新
            repair_progress: {
                fields: {
                    "operatorType": {
                        "value": null,
                        "type": "select",
                        "title": "操作类型",
                        "name": [
                            "operatorType"
                        ],
                        "required": true,
                        "options": [
                            {
                                "label": "提交记录",
                                "value": "1"
                            },
                            {
                                "label": "结束追踪",
                                "value": "2"
                            }
                        ]
                    },
                    "remark": {
                        "value": null,
                        "type": "textarea",
                        "title": "追踪内容",
                        "name": [
                            "remark"
                        ],
                        "required": true
                    }
                },
                path: "/ngic-app/repair/trace/progressUpdate"
            },
            //追踪单验证
            repair_docheck: {
                fields: {
                    "confirmIsPass": {
                        "value": null,
                        "type": "select",
                        "title": "审批意见",
                        "name": [
                            "confirmIsPass"
                        ],
                        "required": true,
                        "options": [
                            {
                                "label": "通过",
                                "value": "1"
                            },
                            {
                                "label": "不通过",
                                "value": "2"
                            }
                        ]
                    },
                    "confirmDesc": {
                        "value": null,
                        "type": "textarea",
                        "title": "审批内容",
                        "name": [
                            "confirmDesc"
                        ],
                        "required": true
                    },
                    // "urlIds": {
                    //     "value": null,
                    //     "type": "upload",
                    //     "title": "上传文件",
                    //     "name": [
                    //         "urlIds"
                    //     ],
                    //     "required": true,
                    //     "listType": "img",
                    //     "multiple": true,
                    //     "limit": 5
                    // }
                },
                path: "/ngic-app/repair/trace/confirm"
            },
            //完成外协
            finish_outside: {
                fields: {
                    "faultReason": {
                        "value": null,
                        "type": "input",
                        "title": "故障原因",
                        "name": [
                            "faultReason"
                        ],
                        "required": true
                    },
                    "repairContent": {
                        "value": null,
                        "type": "input",
                        "title": "维修内容",
                        "name": [
                            "repairContent"
                        ],
                        "required": true
                    },
                    "helpCost": {
                        "value": null,
                        "type": "input",
                        "title": "外协费用",
                        "inputtype": "number",
                        "name": [
                            "helpCost"
                        ],
                        "required": true
                    },
                    "repairTime": {
                        "value": null,
                        "type": "input",
                        "inputtype": "number",
                        "title": "外协时长",
                        "name": [
                            "repairTime"
                        ],
                        "required": true,
                    },
                    "remark": {
                        "value": null,
                        "type": "textarea",
                        "title": "备注",
                        "name": [
                            "remark"
                        ],
                        "required": false
                    },
                },
                path: "/ngic-app/repair/help/finishHelp"
            },
            //延期保养
            maintain_delay: {
                fields: {
                    "delayMaintainDate": {
                        "value": null,
                        "type": "datepicker",
                        "title": "延期时间",
                        "name": [
                            "delayMaintainDate"
                        ],
                        "required": true,
                        "fields": datas?.data?.maintainFrequency !== 0 ? "month" : "day",
                        "showTime": false,
                        "start": datas?.data?.planMaintainDate
                    },
                    "delayReason": {
                        "value": null,
                        "type": "input",
                        "title": "延期原因",
                        "name": [
                            "delayReason"
                        ],
                        "required": true
                    }
                },
                path: "/ngic-app/app/equipmentMaintain/applyDelay"
            },
            //完成保养
            maintain_finish: {
                fields: {
                    "itemList": {
                        "value": null,
                        "type": "list",
                        "title": "保养项目",
                        "name": [
                            "itemList"
                        ],
                        "required": true,
                        "hides": table.length == 0,
                        "dataSource": table,
                        "columns": [
                            {
                                "title": "保养项目",
                                "dataIndex": "maintainItemName",
                                "key": "maintainItemName"
                            },
                            {
                                "title": "保养部位",
                                "dataIndex": "maintainPosition",
                                "key": "maintainPosition"
                            },
                            {
                                "title": "保养方法",
                                "dataIndex": "maintainMethod",
                                "key": "maintainMethod"
                            },
                            {
                                "title": "下限值",
                                "dataIndex": "lowerLimit",
                                "key": "lowerLimit"
                            },
                            {
                                "title": "上限值",
                                "dataIndex": "upperLimit",
                                "key": "upperLimit"
                            },
                            {
                                "title": "保养结果",
                                "dataIndex": "judgeResultQualitative",
                                "key": "judgeResultQualitative",
                                "submittype": "judgeType",
                                "todo": true,
                                "judge": [{
                                    key: "judgeResultType",
                                    val: 1,
                                    type: "select"
                                }, {
                                    key: "qualitativeJudgeResult",
                                    val: 2,
                                    type: "input"
                                }],
                                "options": [
                                    {
                                        "label": "正常 ",
                                        "value": 1
                                    },
                                    {
                                        "label": "异常",
                                        "value": 2
                                    }
                                ]
                            },
                            {
                                "title": "备注",
                                "dataIndex": "remark",
                                "key": "remark",
                                "todo": true,
                                "submittype": "judgeType",
                                "judge": [{
                                    key: "remark",
                                    val: 1,
                                    type: "input"
                                }, {
                                    key: "remark",
                                    val: 2,
                                    type: "input"
                                }],
                            },
                        ],
                        "options": [
                            {
                                "label": "",
                                "value": ""
                            }
                        ],
                        "multiple": true
                    },
                    "maintainSpareList": {
                        "value": null,
                        "type": "jumpselect",
                        "title": "非寿命件更换",
                        "path": "/ngic-app/app/spareParts/queryPrivateStock",
                        "params": {
                        },
                        "name": [
                            "maintainSpareList"
                        ],
                        "required": false,
                        "place": "consumeCount"
                    },
                    "equipmentSpareSupplierList": {
                        "value": null,
                        "type": "jumpselect",
                        "title": "寿命件更换",
                        "path": "/ngic-app/app/spareParts/querySpareSupplierPrivate",
                        "params": {
                            equipmentId
                        },
                        "name": [
                            "equipmentSpareSupplierList"
                        ],
                        "required": false,
                        "place": "number"
                    },
                    "remark": {
                        "value": null,
                        "type": "input",
                        "title": "保养备注",
                        "name": [
                            "remark"
                        ],
                        "required": true
                    }
                },
                path: "/ngic-app/app/equipmentMaintain/finishMaintain"
            },
            //延期审批
            maintain_audit: {
                fields: {
                    "auditResultType": {
                        "value": null,
                        "type": "select",
                        "title": "审批意见",
                        "name": [
                            "auditResultType"
                        ],
                        "required": true,
                        "options": [
                            {
                                "label": "通过",
                                "value": 1
                            },
                            {
                                "label": "不通过",
                                "value": 2
                            }
                        ]
                    },
                    "delayReason": {
                        "value": null,
                        "type": "input",
                        "title": "审批内容",
                        "name": [
                            "delayReason"
                        ],
                        "required": true
                    }
                },
                path: "/ngic-app/app/equipmentMaintain/auditDelay"
            },
            //验证完成保养
            maintain_approval: {
                fields: {
                    "approveStatus": {
                        "value": null,
                        "type": "select",
                        "title": "审批意见",
                        "name": [
                            "approveStatus"
                        ],
                        "required": true,
                        "options": [
                            {
                                "label": "通过",
                                "value": 1
                            },
                            {
                                "label": "不通过",
                                "value": 2
                            }
                        ]
                    },
                    "approveContent": {
                        "value": null,
                        "type": "input",
                        "title": "审批内容",
                        "name": [
                            "approveContent"
                        ],
                        "required": true
                    }
                },
                path: "/ngic-app/app/equipmentMaintain/approval"
            },
            //点检延期申请
            inspection_delay: {
                fields: {
                    "delayCheckDate": {
                        "value": null,
                        "type": "datepicker",
                        "title": "延期日期",
                        "name": [
                            "delayCheckDate"
                        ],
                        "required": true,
                        "fields": "day",
                        "showTime": false,
                        "start": datas?.data?.planCheckDate
                    },
                    "delayReasons": {
                        "value": null,
                        "type": "input",
                        "title": "延期原因",
                        "name": [
                            "delayReasons"
                        ],
                        "required": true
                    }
                },
                path: "/ngic-app/wechatwork/equipmentCheck/applyDelay"
            },
            //完成点检
            inspection_finish: {
                fields: {
                    "itemList": {
                        "value": null,
                        "type": "list",
                        "title": "点检项目",
                        "name": [
                            "itemList"
                        ],
                        "required": true,
                        "hides": table.length == 0,
                        "dataSource": table,
                        "columns": [
                            {
                                "title": "点检项目",
                                "dataIndex": "checkItem",
                                "key": "checkItem"
                            },
                            {
                                "title": "点检部位",
                                "dataIndex": "checkPosition",
                                "key": "checkPosition"
                            },
                            {
                                "title": "点检方法",
                                "dataIndex": "maintainMethod",
                                "key": "maintainMethod"
                            },
                            {
                                "title": "下限值",
                                "dataIndex": "lowerLimit",
                                "key": "lowerLimit"
                            },
                            {
                                "title": "上限值",
                                "dataIndex": "upperLimit",
                                "key": "upperLimit"
                            },
                            {
                                "title": "点检结果",
                                "dataIndex": "judgeResultQualitative",
                                "key": "judgeResultQualitative",
                                "submittype": "judgeType",
                                "todo": true,
                                "judge": [{
                                    key: "judgeResultQualitative",
                                    val: 1,
                                    type: "select"
                                }, {
                                    key: "judgeResultRation",
                                    val: 2,
                                    type: "input"
                                }],
                                "options": [
                                    {
                                        "label": "正常",
                                        "value": 1
                                    },
                                    {
                                        "label": "异常",
                                        "value": 2
                                    }
                                ]
                            },
                            {
                                "title": "备注",
                                "dataIndex": "remark",
                                "key": "remark",
                                "todo": true,
                                "submittype": "judgeType",
                                "judge": [{
                                    key: "remark",
                                    val: 1,
                                    type: "input"
                                }, {
                                    key: "remark",
                                    val: 2,
                                    type: "input"
                                }],
                            },
                        ],
                    },
                    "remark": {
                        "value": null,
                        "type": "input",
                        "title": "点检备注",
                        "name": [
                            "remark"
                        ],
                        "required": true
                    }
                },
                path: "/ngic-app/wechatwork/equipmentCheck/finish"
            },
            //延期审批
            inspection_audit: {
                fields: {
                    "approvalResult": {
                        "value": null,
                        "type": "select",
                        "title": "审批意见",
                        "name": [
                            "approvalResult"
                        ],
                        "required": true,
                        "options": [
                            {
                                "label": "通过",
                                "value": 1
                            },
                            {
                                "label": "不通过",
                                "value": 2
                            }
                        ]
                    },
                    "approvalComments": {
                        "value": null,
                        "type": "input",
                        "title": "审批内容",
                        "name": [
                            "approvalComments"
                        ],
                        "required": true
                    }
                },
                path: "/ngic-app/wechatwork/equipmentCheck/auditDelay"
            }

        }

    }, [table, datas])

    return (
        <InitForm
            fields={fields[type].fields ?? {}}
            loading={loading}
            submit={(data, reset) => {
                dosubmit(data)
            }}
        >

        </InitForm>

    );
}

export default ActionForm;