/* eslint-disable import/no-commonjs */
/* eslint-disable import/first */
import React, { useState } from 'react';
import { View, Text, Image } from '@tarojs/components'
import Taro, { useDidShow } from '@tarojs/taro';
import { request } from '../../utils/request';
import { AtBadge } from 'taro-ui'

import "taro-ui/dist/style/components/button.scss" // 按需引入
import "taro-ui/dist/style/components/badge.scss" // 按需引入
import './index.less'


let transet = {
  dorepair: {
    color: "#4facfe",
    realPath: "/repair/pages/repair/index",
    icon: require("../../assets/baoxiu.png")
  },
  torepair: {
    color: "#4facfe",
    realPath: "/repair/pages/torepair/index",
    icon: require("../../assets/todo.png")
  },
  unrepair: {
    color: "#9795f0",
    realPath: "/repair/pages/repairundo/index",
    icon: require("../../assets/undo.png")
  },
  donerepair: {
    color: "#0ba360",
    realPath: "/repair/pages/repairdone/index",
    icon: require("../../assets/done.png")
  },
  tomaintain: {
    color: "#4facfe",
    realPath: "/maintain/pages/maintain/index",
    icon: require("../../assets/todo.png")
  },
  errmaintain: {
    color: "#f5576c",
    realPath: "ErrorMaintain",
    icon: require("../../assets/error.png")
  },
  unmaintain: {
    color: "#9795f0",
    realPath: "/maintain/pages/maintainundo/index",
    icon: require("../../assets/undo.png")
  },
  donemaintain: {
    color: "#0ba360",
    realPath: "/maintain/pages/maintaindone/index",
    icon: require("../../assets/done.png")
  },
  toinspection: {
    color: "#4facfe",
    realPath: "/inspection/pages/inspection/index",
    icon: require("../../assets/todo.png")
  },
  errinspection: {
    color: "#f5576c",
    realPath: "ErrorInspection",
    icon: require("../../assets/error.png")
  },
  uninspection: {
    color: "#9795f0",
    realPath: "/inspection/pages/inspectionundo/index",
    icon: require("../../assets/undo.png")
  },
  doneinspection: {
    color: "#0ba360",
    realPath: "/inspection/pages/inspectiondone/index",
    icon: require("../../assets/done.png")
  },
  frock: {
    color: "#0ba360",
    realPath: "/frock/pages/frock/index",
    icon: require("../../assets/install.png")
  },
  unfrock: {
    color: "#0ba360",
    realPath: "/frock/pages/unfrock/index",
    icon: require("../../assets/uninstall.png")
  },
  appknowledgefile: {
    color: "#6991c7",
    realPath: "/knowledge/pages/knowledge/index",
    icon: require("../../assets/knowledgefile.png")
  },
  appknowledgeexp: {
    color: "#f7c978",
    realPath: "/knowledge/pages/experience/index",
    icon: require("../../assets/repairexp.png")
  }
}
let dataArr = [
  {
    name: "设备维修",
    path: "apprepair",
    routes: [
      {
        name: "报修",
        icon: "tool",
        path: "dorepair",
      },
      {
        name: "我的待办",
        icon: "filetext1",
        path: "torepair",
      },
      {
        name: "未完成",
        icon: "exclamationcircle",
        path: "unrepair",
      },
      {
        name: "已完成",
        icon: "checkcircle",
        path: "donerepair",
      },
    ]
  }, {
    name: "设备保养",
    path: "maintain",
    routes: [
      {
        name: "我的待办",
        icon: "filetext1",
        path: "tomaintain",
      },
      {
        name: "保养异常",
        icon: "closecircle",
        path: "errmaintain",
      },
      {
        name: "未完成",
        icon: "exclamationcircle",
        path: "unmaintain",
      },
      {
        name: "已完成",
        icon: "checkcircle",
        path: "donemaintain",
      },

    ]
  }, {
    name: "设备点检",
    path: "appinspection",
    routes: [
      {
        name: "我的待办",
        icon: "filetext1",
        path: "toinspection",
      },
      {
        name: "点检异常",
        icon: "closecircle",
        path: "errinspection",
      },
      {
        name: "未完成",
        icon: "exclamationcircle",
        path: "uninspection",
      },
      {
        name: "已完成",
        icon: "checkcircle",
        path: "doneinspection",
      },
    ]
  },
  {
    name: "工装管理",
    path: "allfrock",
    routes: [
      {
        name: "安装工装",
        icon: "book",
        path: "frock",
      },
      {
        name: "拆卸工装",
        icon: "book",
        path: "unfrock",
      },
    ]
  },
  {
    name: "知识库",
    path: "appknowledge",
    routes: [
      {
        name: "知识文件",
        icon: "book",
        path: "appknowledgefile",
      },
      {
        name: "维修经验",
        icon: "tag",
        path: "appknowledgeexp",
      },
      {

      },
      {

      }
    ]
  }
]

function Index() {
  const [dataArrs, setdataArrs] = useState(dataArr);
  const [devicenum, setdevicenum] = useState();
  const [num, setnum] = useState({});

  useDidShow(() => {
    //Taro.navigateTo({ url: "/inspection/pages/inspection/index" })
    Taro.qy.login({
      success: async function (res) {
        if (res.code) {
          //发起网络请求
          let { data } = await request("/ngic-app/wechatwork/identify/queryCode2Session", { jsCode: res.code })
          if (data?.data?.userid) {
            Taro.setStorageSync("userkey", data?.data?.userid)
          }

          let result = await request("/ngic-app/wechatwork/identify/checkBind", { wechatworkId: data?.data?.userid })
          if (result?.data?.data?.bind == 2) {
            Taro.navigateTo({ url: "/pages/bind/index" })
          } else {
            Taro.setStorageSync("userinfo", JSON.stringify(result?.data?.data));
            let dataArres = await request("/ngic-app/wechatwork/identify/queryMenu", {});
            setdataArrs(dataArres?.data?.data?.userHavePermList);
            let devices = await request("/ngic-app/app/equipment/queryEquipmentNum", {});
            setdevicenum(devices?.data?.data?.data);

            let maintain = await request("/ngic-app/app/equipmentMaintain/queryAllNum", {});
            setnum(s => ({
              ...s,
              maintain: maintain?.data?.data?.data,
              tomaintain: maintain?.data?.data?.data?.solveTaskNum ?? 0,
              unmaintain: maintain?.data?.data?.data?.undoneTaskNum ?? 0
            }));

            let inspection = await request("/ngic-app/wechatwork/equipmentCheck/queryAllNum", {});
            setnum(s => ({
              ...s,
              inspection: inspection?.data?.data?.data,
              toinspection: inspection?.data?.data?.data?.solveTaskNum,
              uninspection: inspection?.data?.data?.data?.undoneTaskNum
            }));

            let torepair = await request("/ngic-app/repair/my/taskNum", {});
            setnum(s => ({
              ...s,
              torepair: torepair?.data?.data?.taskNum
            }));

            let unrepair = await request("/ngic-app/repair/all/taskNum", {});
            setnum(s => ({
              ...s,
              unrepair: unrepair?.data?.data?.taskNum
            }));

          }

        } else {

        }
      }
    });

  }, [])





  return (
    <View className='index'>
      <View className='topbox'>
        <Text>设备总数</Text>
        <Text style={{ fontSize: 20 }}>{devicenum}</Text>
      </View>
      <View style={{ width: "100%", marginTop: 12 }}>
        {
          dataArrs.map((item, i) => {
            if (item.routes.length > 0 && item.routes.length < 4) {
              for (let is = 0; is < 5 - item.routes.length; is++) {
                item.routes.push({})
              }
            }

            return <View style={{ borderBottom: "1PX solid #f0f0f0", borderBottomWidth: i == dataArrs.length - 1 ? 0 : 1, padding: "10PX 6PX" }} key={i}>
              <Text style={{ marginLeft: 6, marginTop: 6, marginBottom: 10, fontSize: 16 }}>
                {item.name}
              </Text>
              <View style={{ display: "flex", padding: "12PX 0 4PX 0" }}>
                {
                  item.routes && item.routes.map((it) => {
                    let avatarprops = {
                      src: transet[it.path]?.icon,
                      style: {
                        borderRadius: 4,
                        width: 60,
                        height: 60
                      }
                    }
                    if (it.name) {
                      return (
                        <View style={{ flex: 1 }} className='center' key={it.path} onClick={() => {
                          if (it.path == "dorepair") {
                            Taro.showActionSheet({
                              alertText: "请选择报修方式",
                              itemList: [
                                "扫码报修",
                                "手动报修"
                              ],
                              itemColor: transet[it.path].color,
                              success: (result) => {
                                if (result.tapIndex == 0) {
                                  Taro.scanCode({
                                    success: (res) => {
                                      let equipmentId = res?.result;
                                      request("/ngic-app/repair/canApplyRepair", { equipmentId }, (reses) => {
                                        let result = reses.data?.data ?? {};
                                        console.log(result,reses);
                                        if (result.code == -1) {
                                          Taro.showToast({
                                            title: "设备不存在",
                                            icon: "error"
                                          })
                                        } else if (result.code == 0) {
                                          Taro.showToast({
                                            title: "不在当前车间",
                                            icon: "error"
                                          })
                                        } else if (result.code == 1) {
                                          Taro.navigateTo({ url: transet[it.path].realPath + `?equipmentId=${equipmentId}` })
                                        } else if (result.code == 2) {
                                          Taro.showModal({
                                            title: `${result?.repair?.equipmentName}已报修，当前状态为${result?.repair?.repairStatusName}，无法再次报修-维修工单，请知晓！`,
                                            success: function (res) {
                                              if (res.confirm) {
                                                Taro.navigateTo({ url: transet[it.path].realPath + `?equipmentId=${equipmentId}&disabled=1` })
                                              } else if (res.cancel) {
                                              }
                                            }
                                          })
                                          
                                        }
                                      })

                                    }
                                  })
                                } else {
                                  Taro.navigateTo({ url: transet[it.path].realPath })
                                }

                              }
                            })
                            return
                          }
                          if (it.path == "frock") {
                            Taro.showActionSheet({
                              alertText: "请选择安装方式",
                              itemList: [
                                "扫码安装",
                                "选择安装"
                              ],
                              itemColor: transet[it.path].color,
                              success: (result) => {
                                if (result.tapIndex == 0) {
                                  Taro.scanCode({
                                    success: (res) => {
                                      Taro.navigateTo({ url: `/frock/pages/actions/index?equipmentId=${res.result}&type=1` })
                                    }
                                  })
                                } else {
                                  Taro.navigateTo({ url: transet[it.path].realPath })
                                }
                              }
                            })
                            return
                          }
                          if (it.path == "unfrock") {
                            Taro.showActionSheet({
                              alertText: "请选择拆卸方式",
                              itemList: [
                                "扫码拆卸",
                                "选择拆卸"
                              ],
                              itemColor: transet[it.path].color,
                              success: (result) => {
                                if (result.tapIndex == 0) {
                                  Taro.scanCode({
                                    success: (res) => {
                                      Taro.navigateTo({ url: `/frock/pages/actions/index?equipmentId=${res.result}&type=2` })
                                    }
                                  })
                                } else {
                                  Taro.navigateTo({ url: transet[it.path].realPath })
                                }
                              }
                            })
                            return
                          }


                          Taro.navigateTo({ url: transet[it.path].realPath })
                        }}
                        >
                          <View width={80} height={80} center style={{ marginBottom: 6 }}>
                            <AtBadge value={num[it.path]} maxValue={99}>
                              <Image {...avatarprops}></Image>
                            </AtBadge>
                          </View>

                          <Text style={{ fontSize: "14PX", color: "#999" }}>{it.name}</Text>
                        </View>
                      )
                    } else {
                      return <View style={{ flex: 1 }}></View>
                    }

                  })
                }
              </View>
            </View>
          })
        }
      </View>
    </View>
  );
}

export default Index;
