import Taro from '@tarojs/taro';
import { View, Input} from '@tarojs/components';
import React, { useEffect} from 'react';
import AutoList from '../../components/AutoList';
import './index.less';

const keydes = {
    consumeList: "sparePartsNo",//非寿命件 分页
    replaceList: "lifeSparePartsId",//寿命件
    equipmentSpareSupplierList: "equipmentSpareSupplierId",//寿命件
    maintainSpareList: "id"//非寿命件
}

function SelectPage() {
    const { name, title, path, params, defaultvalue, place } = Taro.Current.router.params;
    let defaultval = defaultvalue ? JSON.parse(defaultvalue) : []
    useEffect(() => {
        Taro.setNavigationBarTitle({
            title
        })
    }, [])

    return (
        <View>
            <AutoList
                path={path}
                resetparams={params ? JSON.parse(params) : {}}
                defaultvalue={defaultval}
                haspage={["consumeList"].indexOf(name) != -1}
                ph="请输入备件名称/料号"
                extra={(submit) => <View className='btn' onClick={() => {
                    let pages = Taro.getCurrentPages();
                    let prevPage = pages[pages.length - 2]; //上1个页面
                    console.log(prevPage);
                    prevPage[name] = submit;
                    Taro.navigateBack();
                }}>提交</View>}
                row={(item, i, setsubmitdata, submitdata) => {
                    let onecekey = item.id ? item.id : item.sparePartsNo;
                    return <View className='item'>
                        <View style={{ display: "flex", alignItems: "center", justifyContent: "space-between" }} className='listheader'>
                            <View style={{ flex: 1, overflow: "hidden" }}>{item.sparePartsNo}</View>
                            <View style={{ flex: 1, overflow: "hidden", textOverflow: "ellipsis", whiteSpace: "nowrap", textAlign: "right" }}>{item.sparePartsName}</View>
                        </View>
                        {
                            item.sparePartsDesc && <View style={{ display: "flex", alignItems: "center", justifyContent: "space-between" }} className='list'>
                                <View style={{ flex: 1, overflow: "hidden" }}>备件描述</View>
                                <View style={{ flex: 1, overflow: "hidden", textOverflow: "ellipsis", whiteSpace: "nowrap", textAlign: "right" }}>{item.sparePartsDesc}</View>
                            </View>
                        }
                        {
                            item.position && <View style={{ display: "flex", alignItems: "center", justifyContent: "space-between" }} className='list'>
                                <View style={{ flex: 1, overflow: "hidden" }}>安装部位</View>
                                <View style={{ flex: 1, overflow: "hidden", textOverflow: "ellipsis", whiteSpace: "nowrap", textAlign: "right" }}>{item.position}</View>
                            </View>
                        }
                        {
                            item.cycleDays && <View style={{ display: "flex", alignItems: "center", justifyContent: "space-between" }} className='list'>
                                <View style={{ flex: 1, overflow: "hidden" }}>周期</View>
                                <View style={{ flex: 1, overflow: "hidden", textOverflow: "ellipsis", whiteSpace: "nowrap", textAlign: "right" }}>{item.cycleDays}</View>
                            </View>
                        }
                        {
                            item.number && <View style={{ display: "flex", alignItems: "center", justifyContent: "space-between" }} className='list'>
                                <View style={{ flex: 1, overflow: "hidden" }}>数量</View>
                                <View style={{ flex: 1, overflow: "hidden", textOverflow: "ellipsis", whiteSpace: "nowrap", textAlign: "right" }}>{item.number}</View>
                            </View>
                        }
                        {
                            item.nextReplaceDate && <View style={{ display: "flex", alignItems: "center", justifyContent: "space-between" }} className='list'>
                                <View style={{ flex: 1, overflow: "hidden" }}>下次更换日期</View>
                                <View style={{ flex: 1, overflow: "hidden", textOverflow: "ellipsis", whiteSpace: "nowrap", textAlign: "right" }}>{item.nextReplaceDate}</View>
                            </View>
                        }
                        {
                            item.availableStock && <View style={{ display: "flex", alignItems: "center", justifyContent: "space-between" }} className='list'>
                                <View style={{ flex: 1, overflow: "hidden" }}>可用库存</View>
                                <View style={{ flex: 1, overflow: "hidden", textOverflow: "ellipsis", whiteSpace: "nowrap", textAlign: "right" }}>{item.availableStock}</View>
                            </View>
                        }

                        <View style={{ display: "flex", alignItems: "center", justifyContent: "space-between" }} className='listaction'>
                            <View style={{ overflow: "hidden" }}>消耗数量</View>
                            <View style={{ flex: 1, overflow: "hidden", textOverflow: "ellipsis", whiteSpace: "nowrap", textAlign: "right" }}>
                                <Input
                                    style={{ textAlign: "right", fontSize: 16, width: "100%", color: "#6190E8" }}
                                    placeholder="请输入"
                                    value={(submitdata && submitdata.length > 0) ?
                                        submitdata.filter(it => it[keydes[name]] == onecekey)[0] ? submitdata.filter(it => it[keydes[name]] == onecekey)[0][place]
                                            : null
                                        : null
                                    }
                                    onInput={(e) => {
                                        setsubmitdata((submitdata) => {
                                            let newsubmitdata = JSON.parse(JSON.stringify(submitdata));
                                            if (newsubmitdata.some(it => it[keydes[name]] == onecekey)) {
                                                newsubmitdata = newsubmitdata.map(it => {
                                                    if (it[keydes[name]] == onecekey) {
                                                        it[place] = e.detail.value;
                                                    }
                                                    return it
                                                })

                                                // sparePartsPrivateStockId
                                            } else {
                                                newsubmitdata.push(name == "equipmentSpareSupplierList" ? {
                                                    [keydes[name]]: onecekey,
                                                    [place]: e.detail.value,
                                                    sparePartsPrivateStockId: item.sparePartsPrivateStockId
                                                } : {
                                                    [keydes[name]]: onecekey,
                                                    [place]: e.detail.value
                                                })
                                            }
                                            return newsubmitdata
                                        })
                                    }}
                                //value={''}
                                />
                            </View>
                        </View>

                    </View>
                }}
            />
        </View>
    );
}

export default SelectPage;
