import { useState, useEffect } from 'react';
import { View, Text, Image } from '@tarojs/components';
import { AtList, AtListItem, AtToast } from 'taro-ui';
import Taro from '@tarojs/taro';
import "taro-ui/dist/style/components/list.scss"; // 按需引入
import "taro-ui/dist/style/components/icon.scss"; // 按需引入
import "taro-ui/dist/style/components/toast.scss"; // 按需引入
import './index.less';
import { request } from '../../utils/request';
import { imgurl } from '../../utils/pathconfig';
import { useAsyncEffect } from 'ahooks';




function Center() {
  const [open, setopen] = useState(false);
  const [text, settext] = useState("");
  const [info, setinfo] = useState();

  Taro.useDidShow(() => {
    let userkey = Taro.getStorageSync("userkey");
    request("/ngic-app/wechatwork/identify/checkBind", { wechatworkId: userkey }).then(result=>{
      Taro.setStorageSync("userinfo", JSON.stringify(result?.data?.data));
      setinfo(result?.data?.data)
    })
  })


  return (
    <View className='center'>
      <AtToast isOpened={open} text={text}></AtToast>
      <Image src={require("../../assets/centerbg.png")} style={{ width: "100%", height: "30vh", marginTop: -20 }}></Image>
      <Image
        onClick={() => {
          if (!info?.user?.userPictureUrl) return;
          Taro.previewImage({
            current: imgurl + info?.user?.userPictureUrl,
            urls: [imgurl + info?.user?.userPictureUrl]
          })
        }}
        src={info?.user?.userPictureUrl ? imgurl + info?.user?.userPictureUrl : require("../../assets/mine.png")}
        style={{ width: "32vw", height: "32vw", position: "absolute", top: "18vh", margin: "auto", borderRadius: "60vw", border: "2PX solid #fff", backgroundColor: "rgb(255, 255, 255)" }}>

      </Image>

      <View style={{ marginTop: 60, width: "100%", display: "flex", flexDirection: "column", justifyContent: "center", alignItems: "center" }}>
        <View style={{ fontSize: 20, margin: "12px 0 12px 0", color: "#0170fe" }}>{info?.user?.userName}</View>
        <View style={{ padding: "0px 12px 0px 12px", display: "flex", flexDirection: "column", justifyContent: "center", alignItems: "center" }}>
          <Text style={{ marginBottom: 8, color: "#000" }}>{info?.user?.shopNames}</Text>
          <Text style={{ textAlign: "center", color: "#666", fontSize: "14PX" }}>{info?.user?.sectionNames}</Text>
        </View>



        <View style={{ width: "100%", marginTop: 36 }}>
          <AtList>
            <AtListItem title='个人信息' thumb={require("../../assets/info.png")} onClick={() => {
              Taro.navigateTo({ url: "/pages/centerdetail/index" })
            }} />
            <AtListItem title='解除绑定' onClick={() => {
              request("/ngic-app/wechatwork/identify/liftBind", {}, (res) => {
                if (res.code == "0002") {
                  setopen(true);
                  settext(res.msg);
                  Taro.navigateTo({ url: "/pages/bind/index" })
                }
              })


            }} arrow='right' thumb={require("../../assets/exit.png")} />
          </AtList>
        </View>

      </View>



    </View>
  )
}

export default Center;
