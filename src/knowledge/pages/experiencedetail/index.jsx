import React, { useEffect, useMemo, useState } from 'react';
import { Text, View, Image } from '@tarojs/components';
import AutoList from '../../../components/AutoList';
import Taro from '@tarojs/taro';
import Detail from '../../../components/Detail';
import { useRequest } from 'ahooks';
import { request } from '../../../utils/request';
import { imgurl } from '../../../utils/pathconfig'
import "taro-ui/dist/style/components/tabs.scss" // 按需引入
import "taro-ui/dist/style/components/action-sheet.scss" // 按需引入


const Experiencedetail = ({
    params,
}) => {
    const { id } = Taro.Current.router.params;

    let { data, refresh } = useRequest(() => {
        return request("/ngic-app/app/repairExperience/queryDetail", { id })
    });

    let res = useMemo(() => {
        if (data?.data) {
            return data?.data?.data
        } else {
            return {}
        }
    }, [data]);
    let imgData = useMemo(() => {
        if (data?.data) {
            let imgLength = data?.data?.data?.data?.urlIds.length ?? 0, imgList = data?.data?.data?.data?.urlIds ?? [];
            if (imgLength > 0) {
                let index = imgLength % 3,
                    newLength;
                if (index > 0 && index < 3) {
                    newLength = 3 - index;
                };
                for (let i = 0; i < newLength; i++) {
                    imgList.push({});
                }
            }
            return imgList;
        } else {
            return [];
        }
    }, [data]);
    let columna = [
        {
            "title": "设备型号",
            "dataIndex": "equipmentModelName",
            "key": "equipmentModelName"
        },
        {
            "title": "关键词",
            "dataIndex": "keyWord",
            "key": "keyWord"
        },
        {
            "title": "报警信息",
            "dataIndex": "alarmInformation",
            "key": "alarmInformation"
        },
        {
            "title": "问题描述",
            "dataIndex": "questionDesc",
            "key": "questionDesc"
        },
        {
            "title": "解决方案",
            "dataIndex": "solution",
            "key": "solution"
        }
    ];

    return (
        <View style={{ display: "flex", flexDirection: "column" }}>
            <Detail
                hasSplit={true}
                columns={columna}
                dataSource={
                    res?.data ?? {}
                }
            ></Detail>
            <View style={{ padding: 12 }}>
                <View style={{ color: "#999", fontSize: 16, marginBottom: 10 }}>维修位置图片</View>
                <View style={{ display: "flex", flexFlow: "row wrap", justifyContent: "space-between" }}>
                    {
                        imgData?.map(it => {
                            if (it.url) {
                                return <View onClick={() => {
                                    let current = imgurl + it.url,
                                        urls = imgData.filter(it => it.url).map(itz => imgurl + itz.url);
                                    Taro.previewImage({
                                        current, // 当前显示图片的http链接
                                        urls  // 需要预览的图片http链接列表
                                    })
                                }} style={{ backgroundImage: imgurl + it.url, backgroundRepeat: "no-repeat", backgroundPosition: "center", width: "30vw", height: "30vw", backgroundSize: "cover", marginBottom: 10 }}></View>
                            } else {
                                return <View style={{ width: "30vw", height: "30vw", marginBottom: 10 }}></View>
                            }
                        })
                    }
                </View>
            </View>

        </View>
    )
};

export default Experiencedetail;
