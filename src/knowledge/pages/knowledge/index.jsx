import React, { Component } from 'react';
import { Text, View, Input } from '@tarojs/components';
import AutoList from '../../../components/AutoList';
import { AtIcon } from 'taro-ui';
import Taro from '@tarojs/taro';
import { imgurl } from '../../../utils/pathconfig';
function Knowledge() {
    
    return (
        <View>
            <AutoList
                path="/ngic-app/app/equipmentKnowledge/queryPassList"
                ph="请输入文件名称、编号"
                row={(item, i) => {
                    return <View className='item' onClick={() => {
                        Taro.downloadFile({
                            url: imgurl + item.urlIds[0].url,
                            success: function (res) {
                                let filePath = res.tempFilePath;
                                let fileType = '';
                                let filearr = filePath.split('.');
                                if (res.statusCode === 200) {
                                    Taro.showToast({
                                        title: '下载成功',
                                        icon: 'success',
                                        duration: 2000
                                    })
                                }

                                switch (filearr[filearr.length - 1]) {
                                    case 'doc':
                                        fileType = 'doc'
                                        break;
                                    case 'xls':
                                        fileType = 'xls'
                                        break;
                                    case 'ppt':
                                        fileType = 'ppt'
                                        break;
                                    case 'pdf':
                                        fileType = 'pdf'
                                        break;
                                    case 'docx':
                                        fileType = 'docx'
                                        break;
                                    case 'xlsx':
                                        fileType = 'xlsx'
                                        break;
                                    case 'pptx':
                                        fileType = 'pptx'
                                        break;

                                    default:
                                        fileType = 'else'
                                        break;
                                }

                                if (['png', 'jpg', 'jpeg', 'bmp', 'gif', 'webp', 'svg'].indexOf(fileType) != -1) {
                                    Taro.previewImage({
                                        current: filePath,
                                        urls: [filePath]
                                    })
                                }

                                if (fileType != 'else') {
                                    Taro.openDocument({
                                        filePath: filePath,
                                        fileType: fileType,
                                        success: function (res) {
                                            Taro.showToast({
                                                title: '文档打开成功',
                                                icon: 'success',
                                                duration: 2000
                                            })
                                        },
                                        fail: function (res) {
                                            Taro.showToast({
                                                title: '文档打开失败',
                                                icon: 'none',
                                                duration: 2000
                                            })
                                        },
                                    });
                                }
                            },
                            fail: function (res) {
                                Taro.showToast({
                                    title: '附件保存失败',
                                    icon: "error",
                                    duration: 2000
                                })
                            }
                        })
                    }}>
                        <View>
                            <View style={{ display: "flex" }}>
                                <View style={{ display: "flex", alignItems: "center", marginRight: 8 }}>
                                    <AtIcon value='folder' size='32' color='#6D9AC9'></AtIcon>
                                </View>
                                <View style={{ alignItems: "center", flexDirection: "column" }}>
                                    <View>
                                        <Text style={{ wordBreak: "break-all" }}>{item.knowledgeBaseName}</Text>
                                        <Text>（{item.knowledgeBaseVersion}）</Text>
                                    </View>
                                    <View>
                                        <Text style={{ fontSize: 14, color: "#aaa" }}>{item.documentNo}</Text>
                                    </View>
                                </View>
                            </View>
                            <View>
                                <Text style={{ fontSize: 14, color: "#aaa", marginRight: 10 }}>{item.uploadUserName}</Text>
                                <Text style={{ fontSize: 14, color: "#aaa" }}>{item.updateTime}</Text>
                            </View>
                        </View>
                    </View>
                }}
            />
        </View>
    );
}
export default Knowledge;