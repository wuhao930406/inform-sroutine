import React, { Component } from 'react';
import { Text, View, Input } from '@tarojs/components';
import AutoList from '../../../components/AutoList';
import Taro from '@tarojs/taro';
function Experience() {

    return (
        <View>
            <AutoList
                path="/ngic-app/app/repairExperience/queryPassList"
                ph="请输入设备型号、关键词、报警信息"
                row={(item, i) => {
                    return <View key={i} className='item' onClick={() => {
                        Taro.navigateTo({url:`/knowledge/pages/experiencedetail/index?id=${item.id}`})
                    }}>
                        <View style={{ overflow: "hidden", textOverflow: "ellipsis", whiteSpace: "nowrap" }}>
                            {item.alarmInformation}
                        </View>
                        <View style={{ overflow: "hidden", textOverflow: "ellipsis", whiteSpace: "nowrap", fontSize: 14, color: "#aaa" }}>
                            设备型号：{item.equipmentModelName}
                        </View>
                        <View style={{ overflow: "hidden", textOverflow: "ellipsis", whiteSpace: "nowrap", fontSize: 14, color: "#aaa" }}>
                            关键词：{item.keyWord}
                        </View>
                    </View>
                }}
            />
        </View>
    );
}
export default Experience;