import { request } from "./request";

//设备下拉框
export function devicelist(params) {
  return request("/ngic-app/repair/canApplyRepair/equipmens/selection", params);
}

//下拉框---故障分类
export function destorytype(params) {
  return request(`/ngic-app/repair/faultType/queryByEquipmentId`, params);
}

//下拉框---故障名称
export function destoryname(params) {
  return request(
    `/ngic-app/repair/faultTypeDetail/queryEquipmentFaultId`,
    params
  );
}

//下拉框---故障描述
export function destorydesc(params,fn) {
  return request(
    `/ngic-app/repair/queryFaultTypeDetailById`,
    params,
    fn
  );
}

//下拉框---工厂用户下拉框
export function usershop(params) {
  return request(`/ngic-app/user/selectbox/shop`, params);
}

//下拉框---工厂用户下拉框
export function usershops(params) {
  return request(`/ngic-app/repair/repair/queryRepairUserList`, params);
}
//下拉框---工厂用户下拉框
export function repairtype(params) {
  return request(`/ngic-app/repair/queryRepairType`, params);
}
