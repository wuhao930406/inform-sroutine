import React, { useState, memo } from "react";
import { View, Button } from "@tarojs/components";
import Detail from "../Detail";
import { AtSwipeAction } from "taro-ui";
import "taro-ui/dist/style/components/icon.scss"; // 按需引入

const AddListPicker = ({ onChange, value, item, columns }) => {
  const [selected, setselected] = useState([]);
  let vals = value ?? [];
  return (
    <View>
      {value &&
        value.map((it, i) => {
          return (
            <View
              style={{
                padding: "10px 0",
                borderBottom:
                  i == value.length - 1 ? "none" : "4px solid #f0f0f0",
                borderLeft: `4px solid ${
                  selected.indexOf(it.id) != -1 ? "lightpink" : "transparent"
                }`,
                borderTop: "4px solid #fff",
                marginBottom: 12,
                position: "relative"
              }}
              key={i}
            >
              <View
                onClick={() => {
                  if (selected.indexOf(it.id) != -1) {
                    setselected(selected => {
                      return selected.filter(items => items != it.id);
                    });
                  } else {
                    let newselected = [...selected, it.id];
                    setselected(newselected);
                  }
                }}
                style={{
                  width: "calc(100% - 60px)",
                  height: "100%",
                  position: "absolute",
                  left: 0,
                  top: 0,
                }}
              ></View>
              <Detail
                columns={columns}
                dataSource={it}
                onClick={e => {
                  e.stopPropagation();
                }}
              />
            </View>
          );
        })}
      <View className="center" style={{ marginTop: 20 }}>
        {selected.length > 0 && (
          <Button
            style={{ flex: 1, marginRight: 12 }}
            onClick={() => {
              let newvalue = [...vals];
              onChange(newvalue.filter(it => selected.indexOf(it.id) == -1));
              setselected([]);
            }}
          >
            删除
          </Button>
        )}

        {vals.length < item.maxlength && (
          <Button
            style={{ flex: 1 }}
            type="primary"
            onClick={() => {
              let newvalue = [...vals];
              newvalue.push({
                repairUserId: null,
                workingHours: null,
                id: new Date().getTime()
              });
              onChange(newvalue);
            }}
          >
            添加
          </Button>
        )}
      </View>
    </View>
  );
};

export default memo(AddListPicker);
