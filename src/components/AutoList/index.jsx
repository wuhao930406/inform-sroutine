import Taro, { Component } from "@tarojs/taro";
import { View, Image, Text, Input } from "@tarojs/components";
import ListView, { LazyBlock } from "taro-listview";
import { useState } from 'react'
import { request } from '../../utils/request'
import "./index.less"
import { useAsyncEffect } from "ahooks";
import { AtIcon } from 'taro-ui'
import "taro-ui/dist/style/components/icon.scss"; // 按需引入
const blankList = [];
let pageIndex = 1;




function AutoList({ row, path, ph, refresh, height, resetparams, extra, defaultvalue, haspage }) {
    const [state, setstate] = useState({
        isLoaded: true,
        error: false,
        hasMore: true,
        isEmpty: false,
        list: blankList
    });
    const [search, setsearch] = useState(null);
    const [submitdata, setsubmitdata] = useState(defaultvalue);
    const { isLoaded, error, hasMore, isEmpty, list } = state,
        getData = async (pIndex = pageIndex) => {
            if (pIndex === 1) setstate({ ...state, isLoaded: false });
            let resetextraparams = haspage ? { ...resetparams, pageIndex: pIndex, pageSize: 10, search } : { ...resetparams, search }
            const {
                data: { data }
            } = await request(path, resetparams ? resetextraparams : { pageIndex: pIndex, pageSize: 10, search });
            return { list: data?.dataList ? data?.dataList : data?.page?.list, hasMore: data?.page?.hasNextPage, isLoaded: pIndex === 1 };
        }, pullDownRefresh = async () => {
            pageIndex = 1;
            const res = await getData(1);
            setstate({ ...state, ...res });
        }, onScrollToLower = async fn => {
            console.log(0);
            const { list } = state;
            const { list: newList, hasMore } = await getData(++pageIndex);
            setstate({
                ...state,
                list: list.concat(newList),
                hasMore
            });
            fn();
        };

    useAsyncEffect(async () => {
        if (refresh === false) return;
        pullDownRefresh();

    }, [search, refresh])



    return (
        <View style={{ display: "flex", flexDirection: "column" }}>
            <View style={{ backgroundColor: "#f0f0f0", display: "flex" }}>
                <View className='center' style={{ padding: 12, flex: 1 }}>
                    <AtIcon value='search' size='24' color='#F00'></AtIcon>
                    <Input value={search} onInput={(e) => { setsearch(e.target.value) }} placeholder={ph ? ph : "请输入设备名称/编号"} style={{ flex: 1, fontSize: "16PX", marginLeft: 6 }}>
                    </Input>
                </View>

                {extra && extra(submitdata)}
            </View>
            <ListView
                isLoaded={isLoaded}
                isError={error}
                hasMore={hasMore}
                color={"red"}
                style={{ height: height ? height : 'calc( 100vh - 50PX)' }}
                isEmpty={isEmpty}
                onPullDownRefresh={pullDownRefresh}
                onScrollToLower={onScrollToLower}
                footerLoadingText={"上滑加载更多..."}
                emptyText={"暂无数据"}
                indicator={{ release: '加载中', activate: '下拉刷新', deactivate: '释放刷新' }}
            >
                {list && list.length > 0 ? list.map((item, index) => {
                    return row(item, index, setsubmitdata, submitdata);
                }) : <View style={{ width: "100vw", height: "120PX", display: "flex", justifyContent: "center", alignItems: "center", color: "#999" }}>
                    {isLoaded ? "暂无数据" : "数据加载中"}
                </View>
                }
            </ListView>
        </View>
    );
}

export default AutoList;