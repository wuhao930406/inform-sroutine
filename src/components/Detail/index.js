import React from "react";
import { Text, View, Card } from "@tarojs/components";

const Detail = ({ columns, dataSource,hasSplit }) => (
  <View style={{borderBottom:hasSplit?"4PX solid #f0f0f0":"none"}}>
    {columns.map((it, i) => {
      return (
        <View
          key={i}
          style={{
            borderBottom:
              i != columns.length - 1 ? "none" : "#f0f0f0 solid 1PX",
            padding: "12PX",
            display: "flex",
            justifyContent: "space-between",
            flexDirection: !it.columns ? "row" : "column",
            fontSize: "16PX",
          }}
        >
          <Text style={{ color: it.todo ? "#ff4800" : "#999",paddingRight:12 }}>
            {it?.title}
          </Text>
          <View
            style={{ flex: 1, display: "flex", justifyContent: "flex-end" }}
          >
            {it?.render ? (
              it?.render(dataSource[it?.key], dataSource)
            ) : (
              <Text style={{ textAlign: "right",wordBreak:"break-all" }}>{dataSource[it?.key]}</Text>
            )}
          </View>
        </View>
      );
    })}
  </View>
);

export default Detail;
