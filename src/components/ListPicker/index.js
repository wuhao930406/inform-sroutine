import React, { useMemo,memo } from 'react';
import { View } from '@tarojs/components';
import Detail from '../Detail';
import _ from 'lodash';

const ListPicker = ({
    onChange,
    value,
    item,
    columns
}) => {

    let { dataSource } = item;
    return (
        dataSource && dataSource.map((it,i)=> {
            return <View style={{padding:"10px 0"}} key={i}>
                <Detail
                    columns={columns}
                    dataSource={it}
                />
            </View>

        })
    )


};

export default memo(ListPicker);
