import React, { useEffect, useMemo, useState } from 'react';
import { View, Picker, Button, Text } from '@tarojs/components';
import Taro, { useDidShow } from '@tarojs/taro';
import { useMouse, useRequest } from 'ahooks';
import { request } from '../../../utils/request';
import Detail from '../../../components/Detail';
import { AtTabs, AtTabsPane, AtActionSheet, AtActionSheetItem } from 'taro-ui';
import "taro-ui/dist/style/components/tabs.scss" // 按需引入
import "taro-ui/dist/style/components/action-sheet.scss" // 按需引入
import dayjs from 'dayjs';
import './index.less'//样式


function Layout({ children, title }) {
  return (<View className='itemdom' key={title}>
    <View className="itemleft">
      <Text className="subbody">
        {title}
      </Text>
    </View>
    <View className="itemright">
      {children}
    </View>
  </View>);
}



let n = 1;

const Actions = ({
  params,
}) => {
  const { type, equipmentId } = Taro.Current.router.params;

  let { data, refresh } = useRequest(async () => {
    const result = await request("/ngic-app/wechatwork/equipmentPart/queryPartDetailsSelect", { equipmentId });
    return result?.data?.data?.data ?? {}
  })

  const [installList, setinstallList] = useState([]);

  let columna = [
    {
      "title": "设备编号",
      "dataIndex": "equipmentNo",
      "key": "equipmentNo"
    },
    {
      "title": "设备名称",
      "dataIndex": "equipmentName",
      "key": "equipmentName"
    }
  ], option = useMemo(() => {
    return data?.partList ?? []
  }, [data])

  useEffect(() => {
    n = 1;
    setinstallList(s => {
      let news = JSON.parse(JSON.stringify(s));
      news.unshift({
        partDetailsId: "",
        toolId: "",
        create: n++
      })

      return news
    })
  }, [])

  let text = type == 1 ? "安装" : "拆卸";

  return (
    <View style={{ display: "flex", flexDirection: "column" }}>
      <View style={{ backgroundColor: "#ff5634", width: "100%", padding: 12 }}>
        <Text style={{ textAlign: "center", color: "#FFFFFF" }}>
          {text}工装
        </Text>
      </View>


      <Detail
        hasSplit={true}
        columns={columna}
        dataSource={data ?? {}}
      ></Detail>

      <View style={{ padding: 8, display: "flex", justifyContent: "space-between", alignItems: "center" }}>
        <Button style={{ flex: 1 }} type='primary' onClick={() => {
          setinstallList(s => {
            let news = JSON.parse(JSON.stringify(s));
            news.unshift({
              partDetailsId: "",
              toolId: "",
              create: n++
            })

            return news
          })
        }}>添加明细({installList?.length})</Button>
        <Button type="warn" style={{ width: 120, marginLeft: 6 }} onClick={() => {
          if (type == "1") {
            request("/ngic-app/wechatwork/equipmentPart/install", {
              partId: data?.partId,
              installList: installList?.map(it => ({
                "partDetailsId": it?.partDetailsId,
                "toolId": it?.toolId
              }))
            }).then(res => {
              if (res.data.code == "0000") {
                wx.showToast({
                  title: "操作成功",
                  icon: "none",
                  duration: 1000
                });
                setTimeout(() => { Taro.navigateBack(); }, 1000)
              }
            })
          } else {
            request("/ngic-app/wechatwork/equipmentPart/dismantle", {
              partId: data?.partId,
              dismantleList: installList?.map(it => (it?.partDetailsId))
            }).then(res => {
              if (res.data.code == "0000") {
                wx.showToast({
                  title: "操作成功",
                  icon: "none",
                  duration: 1000,
                });
                setTimeout(() => { Taro.navigateBack(); }, 1000)
              }
            })
          }
        }}>
          提交
        </Button>

      </View>

      {
        installList && installList?.map((it, i) => {
          return <View style={{ padding: 8, backgroundColor: "#f0f0f0", marginBottom: 12 }}>
            <Layout title={`${text}明细：${it.create}`}>
              {
                installList.length == 1 ? null : <Text style={{ color: "red" }} onClick={() => {
                  setinstallList(s => {
                    let news = JSON.parse(JSON.stringify(s));
                    return news.filter((it, ins) => {
                      return ins !== i
                    })
                  })
                }}>删除</Text>
              }

            </Layout>
            <Layout title={"设备部位"}>
              <Picker
                onChange={(e) => {
                  let index = e.detail.value;
                  let val = option[index].value;
                  request("/ngic-app/wechatwork/equipmentPart/queryPartAndToolSelect", {
                    partDetailsId: val,
                    type
                  }).then(res => {
                    setinstallList(s => {
                      return s?.map?.((item, index) => {
                        if (i == index) {
                          const { partTypeName, currtool, currused, toolList } = res?.data?.data?.data ?? {};
                          item.partDetailsId = val;
                          item.partTypeName = partTypeName;
                          item.currtool = currtool;
                          item.currused = currused;
                          item.toolList = toolList;
                          item.toolId = null;
                          item.lscurrused = null;
                        }
                        return item
                      })
                    })
                  })
                }}
                //style={{ marginTop: 10 }}
                mode={"selector"}
                multiSelector
                rangeKey={"label"}
                range={option ?? []}
              >
                <View className='picker' style={{ color: "#6190E8", fontSize: "18PX" }}>
                  {option ?
                    option.filter((its, i) => {
                      return its.value == it.partDetailsId
                    })[0]?.label ?
                      <Text>
                        {option.filter((its, i) => {
                          return its.value == it.partDetailsId
                        })[0]?.label}
                      </Text> : <Text style={{ color: "rgba(0,0,0,0.5)" }}>请选择</Text>
                    : <Text style={{ color: "rgba(0,0,0,0.5)" }}>请选择</Text>}
                </View>
              </Picker>
            </Layout>
            <Layout title={"部位类型"}>
              <Text>{it?.partTypeName}</Text>
            </Layout>
            <Layout title={"当前工装"}>
              <Text>{it?.currtool}</Text>
            </Layout>
            <Layout title={"已使用情况"}>
              <Text>{it?.currused}</Text>
            </Layout>
            {
              type == 2 ? null : <Layout title={"选择工装"}>
                <Picker
                  onChange={(e) => {
                    let option = it?.toolList ?? [];
                    let index = e.detail.value;
                    let val = option[index].value;
                    request("/ngic-app/wechatwork/equipmentPart/queryToolUsedNum", { toolId: val }).then(res => {
                      setinstallList(s => {
                        return s?.map?.((item, index) => {
                          if (i == index) {
                            item.toolId = val;
                            item.lscurrused = res?.data?.data?.data?.currused;
                          }
                          return item
                        })
                      })
                    })
                  }}
                  //style={{ marginTop: 10 }}
                  mode={"selector"}
                  multiSelector
                  rangeKey={"label"}
                  range={it?.toolList ?? []}
                >
                  <View className='picker' style={{ color: "#6190E8", fontSize: "18PX" }}>
                    {it?.toolList ?
                      it?.toolList.filter((its, i) => {
                        return its.value == it.toolId
                      })[0]?.label ?? <Text style={{ color: "rgba(0,0,0,0.5)" }}>请选择</Text>
                      : <Text style={{ color: "rgba(0,0,0,0.5)" }}>请选择</Text>}
                  </View>
                </Picker>
              </Layout>
            }
            {
              type == 2 ? null : <Layout title={"已使用情况"}>
                <Text>{it?.lscurrused}</Text>
              </Layout>
            }


          </View>
        })
      }


    </View>

  )
};

export default Actions;
