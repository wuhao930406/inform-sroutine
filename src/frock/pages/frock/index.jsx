import React, { useState } from 'react';
import { View, Input } from '@tarojs/components';
import AutoList from '../../../components/AutoList';
import Taro, { useDidShow } from '@tarojs/taro';
import { AtTabs, AtTabsPane, AtIcon } from 'taro-ui'
import "taro-ui/dist/style/components/tabs.scss" // 按需引入
import "taro-ui/dist/style/components/icon.scss"; // 按需引入



const Frock = ({
  params,
}) => {
  const [refresh, setrefresh] = useState();
  const [current, setcurrent] = useState(0);

  //操作返回刷新
  useDidShow(() => {
    let pages = Taro.getCurrentPages();
    let currPage = pages[pages.length - 1];
    const { refresh } = currPage;
    if (refresh) {
      setrefresh(true);
      setrefresh(false);
    }
  })

  let renderFirst = (
    <AutoList
      refresh={refresh}
      path="/ngic-app/wechatwork/equipmentPart/queryEquipmentPartByCurrentUserShop"
      height={"calc(100vh - 106PX)"}
      row={(item, i) => {
        return <View className='item' onClick={() => {
          Taro.navigateTo({ url: `/frock/pages/actions/index?equipmentId=${item.equipmentId}&type=1` })
        }}>
          <View style={{ display: "flex", justifyContent: "space-between", alignItems: "center", borderBottom: "1PX solid #f0f0f0", padding: "0PX 0 10PX 0", marginBottom: 10 }}>
            <View style={{ flex: 1, overflow: "hidden", textOverflow: "ellipsis", whiteSpace: "nowrap" }}>设备:{item.equipmentName}</View>
          </View>
          <View className='center' style={{ flexDirection: "column", width: "100%", fontSize: 15 }}>
            <View className='center' style={{ justifyContent: "space-between", width: "100%" }}>
              <View>
                设备编号:{item.equipmentNo}
              </View>
            </View>
            <View className='center' style={{ justifyContent: "space-between", width: "100%", marginTop: 6, alignItems: "flex-start" }}>
              <View style={{ flex: 1 }}>
                工厂：{item.shopName}
              </View>
              <View style={{ flex: 1, marginLeft: 12, display: "flex", justifyContent: "flex-end" }}>
                车间：{item.sectionName}
              </View>
            </View>

          </View>
        </View>
      }}
    />
  )



  return (
    <View >
      {
        renderFirst
      }
    </View >
  )
};

export default Frock;
