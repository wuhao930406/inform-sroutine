export default {
  pages: [
    "pages/index/index",
    "pages/center/index",
    "pages/centerdetail/index",
    "pages/bind/index",
    "pages/actionform/index",
    "pages/selectpage/index",
  ],
  subpackages: [
    {
      root: "repair",
      pages: [
        "pages/repair/index",
        "pages/repairdetail/index",
        "pages/torepair/index",
        "pages/repairundo/index",
        "pages/repairdone/index",
      ]
    },
    {
      root: "maintain",
      pages: [
        "pages/maintain/index",
        "pages/maintaindetail/index",
        "pages/maintainundo/index",
        "pages/maintaindone/index"
      ]
    },
    {
      root: "inspection",
      pages: [
        "pages/inspection/index",
        "pages/inspectiondetail/index",
        "pages/inspectionundo/index",
        "pages/inspectiondone/index"
      ]
    },
    {
      root: "frock",
      pages: [
        "pages/frock/index",
        "pages/unfrock/index",
        "pages/actions/index",
      ]
    },
    {
      root: "knowledge",
      pages: [
        "pages/knowledge/index",
        "pages/experience/index",
        "pages/experiencedetail/index"
      ]
    }
  ],
  tabBar: {
    list: [
      {
        iconPath: "./assets/home.png",
        selectedIconPath: "./assets/home_on.png",
        pagePath: "pages/index/index",
        text: "首页"
      },
      {
        iconPath: "./assets/user.png",
        selectedIconPath: "./assets/user_on.png",
        pagePath: "pages/center/index",
        text: "我的"
      }
    ],
    color: "#595656",
    selectedColor: "#e60034",
    backgroundColor: "#fff",
    borderStyle: "white"
  },
  window: {
    backgroundTextStyle: "light",
    navigationBarBackgroundColor: "#fff",
    navigationBarTitleText: "WeChat",
    navigationBarTextStyle: "black"
  },

};
